open module com.d4iot.oss.corelib.logging.formatter.event.test {

    requires java.logging;

    requires transitive com.d4iot.oss.corelib.logging.formatter.event;
    requires transitive com.d4iot.oss.corelib.logging.logger;
    requires transitive org.junit.jupiter.engine;
    requires transitive org.junit.jupiter.api;
    requires rest.assured.common;
    requires org.hamcrest;

}
