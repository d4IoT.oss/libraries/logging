package com.d4iot.oss.corelib.logging.formatter.event.test;

import java.time.Instant;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.d4iot.oss.corelib.logging.formatter.event.EventFormatter;
import com.d4iot.oss.corelib.logging.logger.EventLevel;
import com.d4iot.oss.corelib.logging.logger.EventLogger;
import com.d4iot.oss.corelib.logging.logger.EventRecord;
import com.d4iot.oss.corelib.logging.logger.EventSensitivity;
import com.d4iot.oss.corelib.logging.logger.EventType;

public class EventFormatterTest {
    private static int DTF_OFFSET = 19;

    @Test
    void testFormatNoParamsExc() {
        final var eLogger = EventLogger
                .getLogger("TestB1", EventSensitivity.TS_L5,
                           EventSensitivity.TS_L5, EventSensitivity.TS_L5);
        final var evRec = new EventRecord(eLogger, EventLevel.WARNING,
                EventType.APPLICATION, EventSensitivity.SECRET, null,
                "This is a test ", new RuntimeException("TESTEXC"));
        evRec.setInstant(Instant.ofEpochMilli(0));
        evRec.setThreadID(6);
        evRec.setSequenceNumber(9);
        final var evFmt = new EventFormatter();

        final var tmp = evFmt.format(evRec).substring(DTF_OFFSET);
        Assertions.assertTrue(tmp
                .startsWith("[W:A:S][6:9:]TestB1:This is a test \r\nTESTEXC\n"));
    }

    @Test
    void testFormatNoParamsNoExcep() {
        final var eLogger = EventLogger
                .getLogger("Test", "testProps", EventSensitivity.TS_L5,
                           EventSensitivity.TS_L5, EventSensitivity.TS_L5);
        final var evRec = new EventRecord(eLogger, EventLevel.INFO,
                EventType.APPLICATION, EventSensitivity.CONFIDENTIAL,
                "Cor01", "TEST_00");
        evRec.setInstant(Instant.ofEpochMilli(0));
        evRec.setThreadID(1);
        evRec.setSequenceNumber(2);
        final var evFmt = new EventFormatter();

        Assertions.assertEquals("[I:A:C][1:2:Cor01]Test:Message None\r\n",
                                evFmt.format(evRec).substring(DTF_OFFSET));
    }

    @Test
    void testFormatNoParamsQuoteEscape() {
        final var eLogger = EventLogger
                .getLogger("TestB1", EventSensitivity.TS_L5,
                           EventSensitivity.TS_L5, EventSensitivity.TS_L5);
        final var evRec = new EventRecord(eLogger, EventLevel.WARNING,
                EventType.BUSINESS, EventSensitivity.SECRET, "CorB1",
                "This is a test '{' of \\{ and Nothing else");
        evRec.setInstant(Instant.ofEpochMilli(0));
        evRec.setThreadID(6);
        evRec.setSequenceNumber(9);
        final var evFmt = new EventFormatter();

        Assertions
                .assertEquals("[W:B:S][6:9:CorB1]TestB1:This is a test '{' of \\{ and Nothing else\r\n",
                              evFmt.format(evRec).substring(DTF_OFFSET));
    }

    @Test
    void testFormatNoParamsResMissing() {
        final var eLogger = EventLogger
                .getLogger("Test", "testProps", EventSensitivity.TS_L5,
                           EventSensitivity.TS_L5, EventSensitivity.TS_L5);
        final var evRec = new EventRecord(eLogger, EventLevel.INFO,
                EventType.APPLICATION, EventSensitivity.CONFIDENTIAL,
                "Cor01", "TEST_UNKNOWN");
        evRec.setInstant(Instant.ofEpochMilli(0));
        evRec.setThreadID(4);
        evRec.setSequenceNumber(3);
        final var evFmt = new EventFormatter();

        Assertions.assertEquals("[I:A:C][4:3:Cor01]Test:TEST_UNKNOWN\r\n",
                                evFmt.format(evRec).substring(DTF_OFFSET));
    }

    @Test
    void testFormatParams1QuoteEscape() {
        final var eLogger = EventLogger
                .getLogger("TestB1", EventSensitivity.TS_L5,
                           EventSensitivity.TS_L5, EventSensitivity.TS_L5);
        final var evRec = new EventRecord(eLogger, EventLevel.WARNING,
                EventType.BUSINESS, EventSensitivity.SECRET, null,
                "This is a test '{0}''' of \\{0} and {0}", "PARAM1");
        evRec.setInstant(Instant.ofEpochMilli(0));
        evRec.setThreadID(6);
        evRec.setSequenceNumber(9);
        final var evFmt = new EventFormatter();

        Assertions
                .assertEquals("[W:B:S][6:9:]TestB1:This is a test {0}' of {0} and PARAM1\r\n",
                              evFmt.format(evRec).substring(DTF_OFFSET));
    }

    @Test
    void testFormatParamsExc() {
        final var eLogger = EventLogger
                .getLogger("TestB1", EventSensitivity.TS_L5,
                           EventSensitivity.TS_L5, EventSensitivity.TS_L5);
        final var evRec = new EventRecord(eLogger, EventLevel.WARNING,
                EventType.APPLICATION, EventSensitivity.SECRET, null,
                "This is a test {0}", "PARAM0",
                new RuntimeException("TESTEXC"));
        evRec.setInstant(Instant.ofEpochMilli(0));
        evRec.setThreadID(6);
        evRec.setSequenceNumber(9);
        final var evFmt = new EventFormatter();

        final var tmp = evFmt.format(evRec).substring(DTF_OFFSET);
        Assertions.assertTrue(tmp
                .startsWith("[W:A:S][6:9:]TestB1:This is a test PARAM0\r\nTESTEXC\n"));
    }

    @Test
    void testFormatParamsProximityAndNonExist() {
        final var eLogger = EventLogger
                .getLogger("TestB1", EventSensitivity.TS_L5,
                           EventSensitivity.TS_L5, EventSensitivity.TS_L5);
        final var evRec = new EventRecord(eLogger, EventLevel.WARNING,
                EventType.BUSINESS, EventSensitivity.SECRET, null,
                "This is a test {0}{}{2}", "PARAM1");
        evRec.setInstant(Instant.ofEpochMilli(0));
        evRec.setThreadID(6);
        evRec.setSequenceNumber(9);
        final var evFmt = new EventFormatter();

        Assertions
                .assertEquals("[W:B:S][6:9:]TestB1:This is a test PARAM1PARAM1\r\n",
                              evFmt.format(evRec).substring(DTF_OFFSET));
    }

    @Test
    void testFormatParamsProximityOutOfOrder() {
        final var eLogger = EventLogger
                .getLogger("TestB1", EventSensitivity.TS_L5,
                           EventSensitivity.TS_L5, EventSensitivity.TS_L5);
        final var evRec = new EventRecord(eLogger, EventLevel.WARNING,
                EventType.BUSINESS, EventSensitivity.SECRET, null,
                "This is a test {1,}{}{2sddsuyt7^7%}{0}{,}{", "PARAM0",
                "PARAM1", "PARAM2");
        evRec.setInstant(Instant.ofEpochMilli(0));
        evRec.setThreadID(6);
        evRec.setSequenceNumber(9);
        final var evFmt = new EventFormatter();

        Assertions
                .assertEquals("[W:B:S][6:9:]TestB1:This is a test PARAM1PARAM0PARAM2PARAM0PARAM1PARAM2\r\n",
                              evFmt.format(evRec).substring(DTF_OFFSET));
    }
}
