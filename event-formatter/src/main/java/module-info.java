module com.d4iot.oss.corelib.logging.formatter.event {
    requires transitive java.logging;
    requires com.d4iot.oss.corelib.logging.logger;

    exports com.d4iot.oss.corelib.logging.formatter.event;

}
