// Copyright 2021 Lloyd Fernandes <lloyd-oss@astute.biz>
// SPDX-License-Identifier: GPL-3.0-or-later WITH Classpath-exception-2.0
// SPDX-License-Identifier: LicenseRef-d4iot-oss-derived-license
package com.d4iot.oss.corelib.logging.formatter.event;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.MissingResourceException;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

import com.d4iot.oss.corelib.logging.logger.EventRecord;

/**
 * This is a quick and dirty formatter that will only support {} with
 * positional element and not formatting portion. Not using the original
 * code from formatter as it uses StringBuffer and synchronized methods.
 * <br/>
 * DateTime[level:Type:Sensitivity][ThreadId:SequenceNumber:CorrelationId]message<stack
 * trace> <br/>
 * Expect to not use this in prod as prod should got thru handlers to
 * event/message bus.
 */
public class EventFormatter extends Formatter {

    private static final DateTimeFormatter DTF =
            DateTimeFormatter.ISO_LOCAL_DATE_TIME
                    .withZone(ZoneId.systemDefault());
    private static final char COLON = ':';
    private static final char QUOTE = '\'';
    private static final char BRACE_O = '{';
    private static final char BRACE_C = '}';
    private static final char BRACKET_O = '[';
    private static final char BRACKET_C = ']';
    private static final char ESCAPE = '\\';
    private static final char LF = '\n';
    private static final char CR = '\r';

    @Override
    public String format(
            final LogRecord pRecord) {
        final var evRec = (EventRecord) pRecord;
        var msg = evRec.getMessage();
        final var rb = evRec.getResourceBundle();
        if (rb != null) {
            try {
                msg = rb.getString(msg);
            } catch (final MissingResourceException ex) {
                // ignore and use original message
            }
        }
        final var msgArray = msg.toCharArray();
        final var dt = DTF.format(evRec.getInstant()).toCharArray();
        final var sName = evRec.getLoggerShortName().toCharArray();
        final var eLevel =
                evRec.getSeverityLevel().shortName.toCharArray();
        final var eSensitivity =
                evRec.getEventSensitivity().shortName.toCharArray();
        final var eType = evRec.getEventType().shortName.toCharArray();
        final var paramList = evRec.getMessageParams();
        final var paramElems =
                paramList.toArray(new String[paramList.size()]);
        final var est = evRec.getExceptionTrace();
        final var st = (est == null) ? new char[0] : est.toCharArray();
        final var thId =
                Integer.toString(evRec.getThreadID()).toCharArray();
        final var corId = evRec.getCorrelationId();
        final var cor = corId == null ? new char[0] : corId.toCharArray();
        final var seqNum =
                Long.toString(evRec.getSequenceNumber()).toCharArray();
        var newSize =
                msgArray.length + dt.length + sName.length + eLevel.length
                        + eSensitivity.length + eType.length + st.length
                        + cor.length + thId.length + seqNum.length + 32;
        for (final String elem : paramElems) {
            newSize += elem.length();
        }

        final var buf = new char[newSize];
        var tIdx = 0;
        System.arraycopy(dt, 0, buf, tIdx, dt.length);
        tIdx += dt.length;
        buf[tIdx++] = BRACKET_O;
        System.arraycopy(eLevel, 0, buf, tIdx, eLevel.length);
        tIdx += eLevel.length;
        buf[tIdx++] = COLON;
        System.arraycopy(eType, 0, buf, tIdx, eType.length);
        tIdx += eType.length;
        buf[tIdx++] = COLON;
        System.arraycopy(eSensitivity, 0, buf, tIdx, eSensitivity.length);
        tIdx += eSensitivity.length;
        buf[tIdx++] = BRACKET_C;
        buf[tIdx++] = BRACKET_O;
        System.arraycopy(thId, 0, buf, tIdx, thId.length);
        tIdx += thId.length;
        buf[tIdx++] = COLON;
        System.arraycopy(seqNum, 0, buf, tIdx, seqNum.length);
        tIdx += seqNum.length;
        buf[tIdx++] = COLON;
        System.arraycopy(cor, 0, buf, tIdx, cor.length);
        tIdx += cor.length;
        buf[tIdx++] = BRACKET_C;
        System.arraycopy(sName, 0, buf, tIdx, sName.length);
        tIdx += sName.length;
        buf[tIdx++] = COLON;

        tIdx = applyFormat(msgArray, paramElems, buf, tIdx);
        if (est != null) {
            buf[tIdx++] = CR;
            buf[tIdx++] = LF;
            System.arraycopy(st, 0, buf, tIdx, st.length);
            tIdx += st.length;
        }
        buf[tIdx++] = CR;
        buf[tIdx++] = LF;
        return new String(buf, 0, tIdx);

    }

    private int applyFormat(
            final char[] msgArray,
            final String[] paramElems,
            final char[] buf,
            int tIdx) {
        if (paramElems.length > 0) {
            var quoteSkip = false;
            var refSeq = 0;
            for (var i = 0; i < msgArray.length; i++) {
                switch (msgArray[i]) {
                    case BRACE_O:
                        if (quoteSkip) {
                            buf[tIdx++] = msgArray[i];
                        } else {
                            var num = 0;
                            var skip = false;
                            var refId = true;
                            for (++i; (i < msgArray.length)
                                    && (msgArray[i] != BRACE_C); i++) {
                                if (skip) {
                                    continue;
                                }
                                if ((msgArray[i] >= '0')
                                        && (msgArray[i] <= '9')) {
                                    num = (num * 10) + (msgArray[i] - '0');
                                    refId = false;
                                } else {
                                    skip = true;
                                }
                            }
                            if (refId) {
                                num = refSeq++;
                            }
                            if (num < paramElems.length) {
                                final var tmp =
                                        paramElems[num].toCharArray();
                                System.arraycopy(tmp, 0, buf, tIdx,
                                                 tmp.length);
                                tIdx += tmp.length;
                            }
                        }
                        break;
                    case QUOTE:
                        if (msgArray[i + 1] == QUOTE) {
                            buf[tIdx++] = msgArray[i++];
                        } else {
                            quoteSkip = !quoteSkip;
                        }

                        break;
                    case ESCAPE:
                        buf[tIdx++] = msgArray[++i];
                        break;
                    default:
                        buf[tIdx++] = msgArray[i];

                }
            }
        } else {
            System.arraycopy(msgArray, 0, buf, tIdx, msgArray.length);
            tIdx += msgArray.length;
        }
        return tIdx;
    }

}
