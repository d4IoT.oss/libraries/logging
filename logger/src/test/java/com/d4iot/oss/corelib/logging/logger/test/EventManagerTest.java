package com.d4iot.oss.corelib.logging.logger.test;

import java.util.logging.Logger;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.d4iot.oss.corelib.logging.logger.EventLogger;
import com.d4iot.oss.corelib.logging.logger.EventManager;
import com.d4iot.oss.corelib.logging.logger.EventSensitivity;

class EventManagerTest {

    @Test
    void test() {
        final var eMgr = EventManager.getEventManager();
        eMgr.setServiceName("Service");
        Assertions.assertTrue(EventManager.getId()
                .contains("^" + eMgr.getServiceName() + "^"));

    }

    @Test
    void testAddLogger() {
        final var eMgr = EventManager.getEventManager();
        Logger.getLogger("Test");
        final var eLogger = EventLogger
                .getLogger("Test2", EventSensitivity.CONFIDENTIAL,
                           EventSensitivity.FOUO, EventSensitivity.SECRET);
        Assertions.assertSame(eLogger, eMgr.addLogger(eLogger));
    }

}
