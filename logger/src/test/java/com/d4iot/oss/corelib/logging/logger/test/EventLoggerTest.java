package com.d4iot.oss.corelib.logging.logger.test;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.d4iot.oss.corelib.logging.logger.EventLevel;
import com.d4iot.oss.corelib.logging.logger.EventLogger;
import com.d4iot.oss.corelib.logging.logger.EventSensitivity;
import com.d4iot.oss.corelib.logging.logger.EventType;

public class EventLoggerTest {
    public static class MockEventLogger extends EventLogger {
        EventLevel severityLevel;

        EventType type;
        EventSensitivity sensitivty;
        String correlationId;
        String msg;
        Object[] msgFormatValues;

        protected MockEventLogger(final String pName,
                final String pResourceBundleName,
                final EventSensitivity pDefaultSysSensitivity,
                final EventSensitivity pDefaultAppSensitivity,
                final EventSensitivity pDefaultBusSensitivity) {
            super(pName, pResourceBundleName, pDefaultSysSensitivity,
                    pDefaultAppSensitivity, pDefaultBusSensitivity);
        }

        public void assertEvent(
                final String pText,
                final EventLevel pSeverityLevel,
                final EventType pType,
                final EventSensitivity pSensitivty,
                final TestData pData) {

            Assertions.assertEquals(pSeverityLevel, severityLevel, pText);
            Assertions.assertEquals(pType, type, pText);
            Assertions.assertEquals(pSensitivty, sensitivty, pText);
            Assertions.assertEquals(pData.corId, correlationId, pText);
            Assertions.assertEquals(pData.msg, msg, pText);
            Assertions.assertArrayEquals(pData.params, msgFormatValues,
                                         pText);
        }

        @Override
        public void logEvent(
                final EventLevel pSeverityLevel,
                final EventType pType,
                final EventSensitivity pSensitivty,
                final String pCorrelationId,
                final String pMsg,
                final Object... pMsgFormatValues) {
            severityLevel = pSeverityLevel;
            type = pType;
            sensitivty = pSensitivty;
            correlationId = pCorrelationId;
            msg = pMsg;
            msgFormatValues = pMsgFormatValues;
            super.logEvent(pSeverityLevel, pType, pSensitivty,
                           pCorrelationId, pMsg, pMsgFormatValues);
        }
    }

    public static class TestData {
        int seq = 0;
        String msg = "";
        String corId = "";
        Object[] params = new Object[] {"a", "b"};

        void update() {
            msg = "test" + seq++;
            corId = "corId" + seq++;
            params[0] = msg;
            params[1] = corId;

        }
    }

    private static final String NA = "NA";

    @Test
    void testGetSet() {
        final var eLogger = EventLogger
                .getLogger("TestGS", "testProps", EventSensitivity.FOUO,
                           EventSensitivity.CONFIDENTIAL,
                           EventSensitivity.SECRET);
        Assertions.assertEquals("TestGS", eLogger.getShortName());
        Assertions.assertEquals(EventSensitivity.FOUO,
                                eLogger.getDefaultOperationalSensitivty());
        Assertions.assertEquals(EventSensitivity.CONFIDENTIAL,
                                eLogger.getDefaultApplicationSensitivty());
        Assertions.assertEquals(EventSensitivity.SECRET,
                                eLogger.getDefaultBusinessSensitivty());
        eLogger.setDefaultApplicationSensitivty(EventSensitivity.TS);
        eLogger.setDefaultOperationalSensitivty(EventSensitivity.TS_L1);
        eLogger.setDefaultBusinessSensitivty(EventSensitivity.TS_L2);
        Assertions.assertEquals(EventSensitivity.TS_L1,
                                eLogger.getDefaultOperationalSensitivty());
        Assertions.assertEquals(EventSensitivity.TS,
                                eLogger.getDefaultApplicationSensitivty());
        Assertions.assertEquals(EventSensitivity.TS_L2,
                                eLogger.getDefaultBusinessSensitivty());

        EventLogger
                .setDefaultBusinessEventSensitivity(EventSensitivity.TS_L4);
        EventLogger.setDefaultEventSensitivity(EventSensitivity.TS_L3);
        EventLogger.setDefaultEventType(EventType.OPERATIONAL);
        Assertions.assertEquals(EventType.OPERATIONAL,
                                EventLogger.getDefaultEventType());
        Assertions.assertEquals(EventSensitivity.TS_L3,
                                EventLogger.getDefaultEventSensitivity());
        Assertions.assertEquals(EventSensitivity.TS_L4, EventLogger
                .getDefaultBusinessEventSensitivity());

    }

    @Test
    void testLogHelperMethods() {
        final var eLogger = new MockEventLogger("ThisHelper.my.Methods",
                null, EventSensitivity.CONFIDENTIAL,
                EventSensitivity.CONFIDENTIAL,
                EventSensitivity.CONFIDENTIAL);
        final var testData = new TestData();

        Assertions.assertEquals("T.my.Methods", eLogger.getShortName());
        Assertions.assertNotNull(eLogger);

        testData.update();
        eLogger.trace(EventType.SECURITY, EventSensitivity.CONFIDENTIAL,
                      testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("trace", EventLevel.TRACE, EventType.SECURITY,
                            EventSensitivity.CONFIDENTIAL, testData);
        testData.update();
        eLogger.traceAC(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("traceAC", EventLevel.TRACE,
                            EventType.APPLICATION,
                            EventSensitivity.CONFIDENTIAL, testData);
        testData.update();
        eLogger.traceAF(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("traceAF", EventLevel.TRACE,
                            EventType.APPLICATION, EventSensitivity.FOUO,
                            testData);
        testData.update();
        eLogger.traceAS(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("traceAS", EventLevel.TRACE,
                            EventType.APPLICATION, EventSensitivity.SECRET,
                            testData);
        testData.update();
        eLogger.traceAT(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("traceAT", EventLevel.TRACE,
                            EventType.APPLICATION, EventSensitivity.TS,
                            testData);
        testData.update();
        eLogger.traceAU(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("traceAU", EventLevel.TRACE,
                            EventType.APPLICATION,
                            EventSensitivity.UNCLASSIFIED, testData);
        testData.update();
        eLogger.traceBC(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("traceBC", EventLevel.TRACE,
                            EventType.BUSINESS,
                            EventSensitivity.CONFIDENTIAL, testData);
        testData.update();
        eLogger.traceBF(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("traceBF", EventLevel.TRACE,
                            EventType.BUSINESS, EventSensitivity.FOUO,
                            testData);
        testData.update();
        eLogger.traceBS(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("traceBS", EventLevel.TRACE,
                            EventType.BUSINESS, EventSensitivity.SECRET,
                            testData);
        testData.update();
        eLogger.traceBT(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("traceBT", EventLevel.TRACE,
                            EventType.BUSINESS, EventSensitivity.TS,
                            testData);
        testData.update();
        eLogger.traceBU(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("traceBU", EventLevel.TRACE,
                            EventType.BUSINESS,
                            EventSensitivity.UNCLASSIFIED, testData);

        testData.update();
        eLogger.traceDC(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("traceDC", EventLevel.TRACE, EventType.DATA,
                            EventSensitivity.CONFIDENTIAL, testData);
        testData.update();
        eLogger.traceDF(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("traceDF", EventLevel.TRACE, EventType.DATA,
                            EventSensitivity.FOUO, testData);
        testData.update();
        eLogger.traceDS(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("traceDS", EventLevel.TRACE, EventType.DATA,
                            EventSensitivity.SECRET, testData);
        testData.update();
        eLogger.traceDT(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("traceDT", EventLevel.TRACE, EventType.DATA,
                            EventSensitivity.TS, testData);
        testData.update();
        eLogger.traceDU(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("traceDU", EventLevel.TRACE, EventType.DATA,
                            EventSensitivity.UNCLASSIFIED, testData);

        testData.update();
        eLogger.traceOC(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("traceOC", EventLevel.TRACE,
                            EventType.OPERATIONAL,
                            EventSensitivity.CONFIDENTIAL, testData);
        testData.update();
        eLogger.traceOF(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("traceOF", EventLevel.TRACE,
                            EventType.OPERATIONAL, EventSensitivity.FOUO,
                            testData);
        testData.update();
        eLogger.traceOS(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("traceOS", EventLevel.TRACE,
                            EventType.OPERATIONAL, EventSensitivity.SECRET,
                            testData);
        testData.update();
        eLogger.traceOT(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("traceOT", EventLevel.TRACE,
                            EventType.OPERATIONAL, EventSensitivity.TS,
                            testData);
        testData.update();
        eLogger.traceOU(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("traceOU", EventLevel.TRACE,
                            EventType.OPERATIONAL,
                            EventSensitivity.UNCLASSIFIED, testData);
        testData.update();
        eLogger.traceSC(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("traceSC", EventLevel.TRACE,
                            EventType.SECURITY,
                            EventSensitivity.CONFIDENTIAL, testData);
        testData.update();
        eLogger.traceSF(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("traceSF", EventLevel.TRACE,
                            EventType.SECURITY, EventSensitivity.FOUO,
                            testData);
        testData.update();
        eLogger.traceSS(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("traceSS", EventLevel.TRACE,
                            EventType.SECURITY, EventSensitivity.SECRET,
                            testData);
        testData.update();
        eLogger.traceST(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("traceST", EventLevel.TRACE,
                            EventType.SECURITY, EventSensitivity.TS,
                            testData);
        testData.update();
        eLogger.traceSU(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("traceSU", EventLevel.TRACE,
                            EventType.SECURITY,
                            EventSensitivity.UNCLASSIFIED, testData);

        testData.update();
        eLogger.debug(EventType.SECURITY, EventSensitivity.CONFIDENTIAL,
                      testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("debug", EventLevel.DEBUG, EventType.SECURITY,
                            EventSensitivity.CONFIDENTIAL, testData);
        testData.update();
        eLogger.debugAC(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("debugAC", EventLevel.DEBUG,
                            EventType.APPLICATION,
                            EventSensitivity.CONFIDENTIAL, testData);
        testData.update();
        eLogger.debugAF(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("debugAF", EventLevel.DEBUG,
                            EventType.APPLICATION, EventSensitivity.FOUO,
                            testData);
        testData.update();
        eLogger.debugAS(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("debugAS", EventLevel.DEBUG,
                            EventType.APPLICATION, EventSensitivity.SECRET,
                            testData);
        testData.update();
        eLogger.debugAT(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("debugAT", EventLevel.DEBUG,
                            EventType.APPLICATION, EventSensitivity.TS,
                            testData);
        testData.update();
        eLogger.debugAU(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("debugAU", EventLevel.DEBUG,
                            EventType.APPLICATION,
                            EventSensitivity.UNCLASSIFIED, testData);
        testData.update();
        eLogger.debugBC(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("debugBC", EventLevel.DEBUG,
                            EventType.BUSINESS,
                            EventSensitivity.CONFIDENTIAL, testData);
        testData.update();
        eLogger.debugBF(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("debugBF", EventLevel.DEBUG,
                            EventType.BUSINESS, EventSensitivity.FOUO,
                            testData);
        testData.update();
        eLogger.debugBS(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("debugBS", EventLevel.DEBUG,
                            EventType.BUSINESS, EventSensitivity.SECRET,
                            testData);
        testData.update();
        eLogger.debugBT(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("debugBT", EventLevel.DEBUG,
                            EventType.BUSINESS, EventSensitivity.TS,
                            testData);
        testData.update();
        eLogger.debugBU(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("debugBU", EventLevel.DEBUG,
                            EventType.BUSINESS,
                            EventSensitivity.UNCLASSIFIED, testData);

        testData.update();
        eLogger.debugDC(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("debugDC", EventLevel.DEBUG, EventType.DATA,
                            EventSensitivity.CONFIDENTIAL, testData);
        testData.update();
        eLogger.debugDF(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("debugDF", EventLevel.DEBUG, EventType.DATA,
                            EventSensitivity.FOUO, testData);
        testData.update();
        eLogger.debugDS(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("debugDS", EventLevel.DEBUG, EventType.DATA,
                            EventSensitivity.SECRET, testData);
        testData.update();
        eLogger.debugDT(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("debugDT", EventLevel.DEBUG, EventType.DATA,
                            EventSensitivity.TS, testData);
        testData.update();
        eLogger.debugDU(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("debugDU", EventLevel.DEBUG, EventType.DATA,
                            EventSensitivity.UNCLASSIFIED, testData);

        testData.update();
        eLogger.debugOC(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("debugOC", EventLevel.DEBUG,
                            EventType.OPERATIONAL,
                            EventSensitivity.CONFIDENTIAL, testData);
        testData.update();
        eLogger.debugOF(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("debugOF", EventLevel.DEBUG,
                            EventType.OPERATIONAL, EventSensitivity.FOUO,
                            testData);
        testData.update();
        eLogger.debugOS(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("debugOS", EventLevel.DEBUG,
                            EventType.OPERATIONAL, EventSensitivity.SECRET,
                            testData);
        testData.update();
        eLogger.debugOT(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("debugOT", EventLevel.DEBUG,
                            EventType.OPERATIONAL, EventSensitivity.TS,
                            testData);
        testData.update();
        eLogger.debugOU(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("debugOU", EventLevel.DEBUG,
                            EventType.OPERATIONAL,
                            EventSensitivity.UNCLASSIFIED, testData);
        testData.update();
        eLogger.debugSC(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("debugSC", EventLevel.DEBUG,
                            EventType.SECURITY,
                            EventSensitivity.CONFIDENTIAL, testData);
        testData.update();
        eLogger.debugSF(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("debugSF", EventLevel.DEBUG,
                            EventType.SECURITY, EventSensitivity.FOUO,
                            testData);
        testData.update();
        eLogger.debugSS(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("debugSS", EventLevel.DEBUG,
                            EventType.SECURITY, EventSensitivity.SECRET,
                            testData);
        testData.update();
        eLogger.debugST(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("debugST", EventLevel.DEBUG,
                            EventType.SECURITY, EventSensitivity.TS,
                            testData);
        testData.update();
        eLogger.debugSU(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("debugSU", EventLevel.DEBUG,
                            EventType.SECURITY,
                            EventSensitivity.UNCLASSIFIED, testData);

        testData.update();
        eLogger.config(EventType.SECURITY, EventSensitivity.CONFIDENTIAL,
                       testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("config", EventLevel.CONFIG,
                            EventType.SECURITY,
                            EventSensitivity.CONFIDENTIAL, testData);
        testData.update();
        eLogger.configAC(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("configAC", EventLevel.CONFIG,
                            EventType.APPLICATION,
                            EventSensitivity.CONFIDENTIAL, testData);
        testData.update();
        eLogger.configAF(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("configAF", EventLevel.CONFIG,
                            EventType.APPLICATION, EventSensitivity.FOUO,
                            testData);
        testData.update();
        eLogger.configAS(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("configAS", EventLevel.CONFIG,
                            EventType.APPLICATION, EventSensitivity.SECRET,
                            testData);
        testData.update();
        eLogger.configAT(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("configAT", EventLevel.CONFIG,
                            EventType.APPLICATION, EventSensitivity.TS,
                            testData);
        testData.update();
        eLogger.configAU(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("configAU", EventLevel.CONFIG,
                            EventType.APPLICATION,
                            EventSensitivity.UNCLASSIFIED, testData);
        testData.update();
        eLogger.configBC(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("configBC", EventLevel.CONFIG,
                            EventType.BUSINESS,
                            EventSensitivity.CONFIDENTIAL, testData);
        testData.update();
        eLogger.configBF(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("configBF", EventLevel.CONFIG,
                            EventType.BUSINESS, EventSensitivity.FOUO,
                            testData);
        testData.update();
        eLogger.configBS(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("configBS", EventLevel.CONFIG,
                            EventType.BUSINESS, EventSensitivity.SECRET,
                            testData);
        testData.update();
        eLogger.configBT(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("configBT", EventLevel.CONFIG,
                            EventType.BUSINESS, EventSensitivity.TS,
                            testData);
        testData.update();
        eLogger.configBU(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("configBU", EventLevel.CONFIG,
                            EventType.BUSINESS,
                            EventSensitivity.UNCLASSIFIED, testData);

        testData.update();
        eLogger.configDC(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("configDC", EventLevel.CONFIG, EventType.DATA,
                            EventSensitivity.CONFIDENTIAL, testData);
        testData.update();
        eLogger.configDF(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("configDF", EventLevel.CONFIG, EventType.DATA,
                            EventSensitivity.FOUO, testData);
        testData.update();
        eLogger.configDS(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("configDS", EventLevel.CONFIG, EventType.DATA,
                            EventSensitivity.SECRET, testData);
        testData.update();
        eLogger.configDT(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("configDT", EventLevel.CONFIG, EventType.DATA,
                            EventSensitivity.TS, testData);
        testData.update();
        eLogger.configDU(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("configDU", EventLevel.CONFIG, EventType.DATA,
                            EventSensitivity.UNCLASSIFIED, testData);

        testData.update();
        eLogger.configOC(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("configOC", EventLevel.CONFIG,
                            EventType.OPERATIONAL,
                            EventSensitivity.CONFIDENTIAL, testData);
        testData.update();
        eLogger.configOF(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("configOF", EventLevel.CONFIG,
                            EventType.OPERATIONAL, EventSensitivity.FOUO,
                            testData);
        testData.update();
        eLogger.configOS(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("configOS", EventLevel.CONFIG,
                            EventType.OPERATIONAL, EventSensitivity.SECRET,
                            testData);
        testData.update();
        eLogger.configOT(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("configOT", EventLevel.CONFIG,
                            EventType.OPERATIONAL, EventSensitivity.TS,
                            testData);
        testData.update();
        eLogger.configOU(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("configOU", EventLevel.CONFIG,
                            EventType.OPERATIONAL,
                            EventSensitivity.UNCLASSIFIED, testData);
        testData.update();
        eLogger.configSC(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("configSC", EventLevel.CONFIG,
                            EventType.SECURITY,
                            EventSensitivity.CONFIDENTIAL, testData);
        testData.update();
        eLogger.configSF(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("configSF", EventLevel.CONFIG,
                            EventType.SECURITY, EventSensitivity.FOUO,
                            testData);
        testData.update();
        eLogger.configSS(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("configSS", EventLevel.CONFIG,
                            EventType.SECURITY, EventSensitivity.SECRET,
                            testData);
        testData.update();
        eLogger.configST(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("configST", EventLevel.CONFIG,
                            EventType.SECURITY, EventSensitivity.TS,
                            testData);
        testData.update();
        eLogger.configSU(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("configSU", EventLevel.CONFIG,
                            EventType.SECURITY,
                            EventSensitivity.UNCLASSIFIED, testData);

        testData.update();
        eLogger.info(EventType.SECURITY, EventSensitivity.CONFIDENTIAL,
                     testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("info", EventLevel.INFO, EventType.SECURITY,
                            EventSensitivity.CONFIDENTIAL, testData);
        testData.update();
        eLogger.infoAC(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("infoAC", EventLevel.INFO,
                            EventType.APPLICATION,
                            EventSensitivity.CONFIDENTIAL, testData);
        testData.update();
        eLogger.infoAF(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("infoAF", EventLevel.INFO,
                            EventType.APPLICATION, EventSensitivity.FOUO,
                            testData);
        testData.update();
        eLogger.infoAS(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("infoAS", EventLevel.INFO,
                            EventType.APPLICATION, EventSensitivity.SECRET,
                            testData);
        testData.update();
        eLogger.infoAT(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("infoAT", EventLevel.INFO,
                            EventType.APPLICATION, EventSensitivity.TS,
                            testData);
        testData.update();
        eLogger.infoAU(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("infoAU", EventLevel.INFO,
                            EventType.APPLICATION,
                            EventSensitivity.UNCLASSIFIED, testData);
        testData.update();
        eLogger.infoBC(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("infoBC", EventLevel.INFO, EventType.BUSINESS,
                            EventSensitivity.CONFIDENTIAL, testData);
        testData.update();
        eLogger.infoBF(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("infoBF", EventLevel.INFO, EventType.BUSINESS,
                            EventSensitivity.FOUO, testData);
        testData.update();
        eLogger.infoBS(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("infoBS", EventLevel.INFO, EventType.BUSINESS,
                            EventSensitivity.SECRET, testData);
        testData.update();
        eLogger.infoBT(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("infoBT", EventLevel.INFO, EventType.BUSINESS,
                            EventSensitivity.TS, testData);
        testData.update();
        eLogger.infoBU(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("infoBU", EventLevel.INFO, EventType.BUSINESS,
                            EventSensitivity.UNCLASSIFIED, testData);

        testData.update();
        eLogger.infoDC(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("infoDC", EventLevel.INFO, EventType.DATA,
                            EventSensitivity.CONFIDENTIAL, testData);
        testData.update();
        eLogger.infoDF(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("infoDF", EventLevel.INFO, EventType.DATA,
                            EventSensitivity.FOUO, testData);
        testData.update();
        eLogger.infoDS(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("infoDS", EventLevel.INFO, EventType.DATA,
                            EventSensitivity.SECRET, testData);
        testData.update();
        eLogger.infoDT(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("infoDT", EventLevel.INFO, EventType.DATA,
                            EventSensitivity.TS, testData);
        testData.update();
        eLogger.infoDU(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("infoDU", EventLevel.INFO, EventType.DATA,
                            EventSensitivity.UNCLASSIFIED, testData);

        testData.update();
        eLogger.infoOC(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("infoOC", EventLevel.INFO,
                            EventType.OPERATIONAL,
                            EventSensitivity.CONFIDENTIAL, testData);
        testData.update();
        eLogger.infoOF(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("infoOF", EventLevel.INFO,
                            EventType.OPERATIONAL, EventSensitivity.FOUO,
                            testData);
        testData.update();
        eLogger.infoOS(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("infoOS", EventLevel.INFO,
                            EventType.OPERATIONAL, EventSensitivity.SECRET,
                            testData);
        testData.update();
        eLogger.infoOT(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("infoOT", EventLevel.INFO,
                            EventType.OPERATIONAL, EventSensitivity.TS,
                            testData);
        testData.update();
        eLogger.infoOU(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("infoOU", EventLevel.INFO,
                            EventType.OPERATIONAL,
                            EventSensitivity.UNCLASSIFIED, testData);
        testData.update();
        eLogger.infoSC(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("infoSC", EventLevel.INFO, EventType.SECURITY,
                            EventSensitivity.CONFIDENTIAL, testData);
        testData.update();
        eLogger.infoSF(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("infoSF", EventLevel.INFO, EventType.SECURITY,
                            EventSensitivity.FOUO, testData);
        testData.update();
        eLogger.infoSS(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("infoSS", EventLevel.INFO, EventType.SECURITY,
                            EventSensitivity.SECRET, testData);
        testData.update();
        eLogger.infoST(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("infoST", EventLevel.INFO, EventType.SECURITY,
                            EventSensitivity.TS, testData);
        testData.update();
        eLogger.infoSU(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("infoSU", EventLevel.INFO, EventType.SECURITY,
                            EventSensitivity.UNCLASSIFIED, testData);

        testData.update();
        eLogger.warn(EventType.SECURITY, EventSensitivity.CONFIDENTIAL,
                     testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("warn", EventLevel.WARNING, EventType.SECURITY,
                            EventSensitivity.CONFIDENTIAL, testData);
        testData.update();
        eLogger.warnAC(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("warnAC", EventLevel.WARNING,
                            EventType.APPLICATION,
                            EventSensitivity.CONFIDENTIAL, testData);
        testData.update();
        eLogger.warnAF(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("warnAF", EventLevel.WARNING,
                            EventType.APPLICATION, EventSensitivity.FOUO,
                            testData);
        testData.update();
        eLogger.warnAS(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("warnAS", EventLevel.WARNING,
                            EventType.APPLICATION, EventSensitivity.SECRET,
                            testData);
        testData.update();
        eLogger.warnAT(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("warnAT", EventLevel.WARNING,
                            EventType.APPLICATION, EventSensitivity.TS,
                            testData);
        testData.update();
        eLogger.warnAU(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("warnAU", EventLevel.WARNING,
                            EventType.APPLICATION,
                            EventSensitivity.UNCLASSIFIED, testData);
        testData.update();
        eLogger.warnBC(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("warnBC", EventLevel.WARNING,
                            EventType.BUSINESS,
                            EventSensitivity.CONFIDENTIAL, testData);
        testData.update();
        eLogger.warnBF(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("warnBF", EventLevel.WARNING,
                            EventType.BUSINESS, EventSensitivity.FOUO,
                            testData);
        testData.update();
        eLogger.warnBS(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("warnBS", EventLevel.WARNING,
                            EventType.BUSINESS, EventSensitivity.SECRET,
                            testData);
        testData.update();
        eLogger.warnBT(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("warnBT", EventLevel.WARNING,
                            EventType.BUSINESS, EventSensitivity.TS,
                            testData);
        testData.update();
        eLogger.warnBU(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("warnBU", EventLevel.WARNING,
                            EventType.BUSINESS,
                            EventSensitivity.UNCLASSIFIED, testData);

        testData.update();
        eLogger.warnDC(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("warnDC", EventLevel.WARNING, EventType.DATA,
                            EventSensitivity.CONFIDENTIAL, testData);
        testData.update();
        eLogger.warnDF(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("warnDF", EventLevel.WARNING, EventType.DATA,
                            EventSensitivity.FOUO, testData);
        testData.update();
        eLogger.warnDS(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("warnDS", EventLevel.WARNING, EventType.DATA,
                            EventSensitivity.SECRET, testData);
        testData.update();
        eLogger.warnDT(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("warnDT", EventLevel.WARNING, EventType.DATA,
                            EventSensitivity.TS, testData);
        testData.update();
        eLogger.warnDU(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("warnDU", EventLevel.WARNING, EventType.DATA,
                            EventSensitivity.UNCLASSIFIED, testData);

        testData.update();
        eLogger.warnOC(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("warnOC", EventLevel.WARNING,
                            EventType.OPERATIONAL,
                            EventSensitivity.CONFIDENTIAL, testData);
        testData.update();
        eLogger.warnOF(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("warnOF", EventLevel.WARNING,
                            EventType.OPERATIONAL, EventSensitivity.FOUO,
                            testData);
        testData.update();
        eLogger.warnOS(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("warnOS", EventLevel.WARNING,
                            EventType.OPERATIONAL, EventSensitivity.SECRET,
                            testData);
        testData.update();
        eLogger.warnOT(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("warnOT", EventLevel.WARNING,
                            EventType.OPERATIONAL, EventSensitivity.TS,
                            testData);
        testData.update();
        eLogger.warnOU(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("warnOU", EventLevel.WARNING,
                            EventType.OPERATIONAL,
                            EventSensitivity.UNCLASSIFIED, testData);
        testData.update();
        eLogger.warnSC(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("warnSC", EventLevel.WARNING,
                            EventType.SECURITY,
                            EventSensitivity.CONFIDENTIAL, testData);
        testData.update();
        eLogger.warnSF(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("warnSF", EventLevel.WARNING,
                            EventType.SECURITY, EventSensitivity.FOUO,
                            testData);
        testData.update();
        eLogger.warnSS(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("warnSS", EventLevel.WARNING,
                            EventType.SECURITY, EventSensitivity.SECRET,
                            testData);
        testData.update();
        eLogger.warnST(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("warnST", EventLevel.WARNING,
                            EventType.SECURITY, EventSensitivity.TS,
                            testData);
        testData.update();
        eLogger.warnSU(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("warnSU", EventLevel.WARNING,
                            EventType.SECURITY,
                            EventSensitivity.UNCLASSIFIED, testData);

        testData.update();
        eLogger.error(EventType.SECURITY, EventSensitivity.CONFIDENTIAL,
                      testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("error", EventLevel.ERROR, EventType.SECURITY,
                            EventSensitivity.CONFIDENTIAL, testData);
        testData.update();
        eLogger.errorAC(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("errorAC", EventLevel.ERROR,
                            EventType.APPLICATION,
                            EventSensitivity.CONFIDENTIAL, testData);
        testData.update();
        eLogger.errorAF(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("errorAF", EventLevel.ERROR,
                            EventType.APPLICATION, EventSensitivity.FOUO,
                            testData);
        testData.update();
        eLogger.errorAS(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("errorAS", EventLevel.ERROR,
                            EventType.APPLICATION, EventSensitivity.SECRET,
                            testData);
        testData.update();
        eLogger.errorAT(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("errorAT", EventLevel.ERROR,
                            EventType.APPLICATION, EventSensitivity.TS,
                            testData);
        testData.update();
        eLogger.errorAU(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("errorAU", EventLevel.ERROR,
                            EventType.APPLICATION,
                            EventSensitivity.UNCLASSIFIED, testData);
        testData.update();
        eLogger.errorBC(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("errorBC", EventLevel.ERROR,
                            EventType.BUSINESS,
                            EventSensitivity.CONFIDENTIAL, testData);
        testData.update();
        eLogger.errorBF(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("errorBF", EventLevel.ERROR,
                            EventType.BUSINESS, EventSensitivity.FOUO,
                            testData);
        testData.update();
        eLogger.errorBS(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("errorBS", EventLevel.ERROR,
                            EventType.BUSINESS, EventSensitivity.SECRET,
                            testData);
        testData.update();
        eLogger.errorBT(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("errorBT", EventLevel.ERROR,
                            EventType.BUSINESS, EventSensitivity.TS,
                            testData);
        testData.update();
        eLogger.errorBU(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("errorBU", EventLevel.ERROR,
                            EventType.BUSINESS,
                            EventSensitivity.UNCLASSIFIED, testData);

        testData.update();
        eLogger.errorDC(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("errorDC", EventLevel.ERROR, EventType.DATA,
                            EventSensitivity.CONFIDENTIAL, testData);
        testData.update();
        eLogger.errorDF(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("errorDF", EventLevel.ERROR, EventType.DATA,
                            EventSensitivity.FOUO, testData);
        testData.update();
        eLogger.errorDS(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("errorDS", EventLevel.ERROR, EventType.DATA,
                            EventSensitivity.SECRET, testData);
        testData.update();
        eLogger.errorDT(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("errorDT", EventLevel.ERROR, EventType.DATA,
                            EventSensitivity.TS, testData);
        testData.update();
        eLogger.errorDU(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("errorDU", EventLevel.ERROR, EventType.DATA,
                            EventSensitivity.UNCLASSIFIED, testData);

        testData.update();
        eLogger.errorOC(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("errorOC", EventLevel.ERROR,
                            EventType.OPERATIONAL,
                            EventSensitivity.CONFIDENTIAL, testData);
        testData.update();
        eLogger.errorOF(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("errorOF", EventLevel.ERROR,
                            EventType.OPERATIONAL, EventSensitivity.FOUO,
                            testData);
        testData.update();
        eLogger.errorOS(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("errorOS", EventLevel.ERROR,
                            EventType.OPERATIONAL, EventSensitivity.SECRET,
                            testData);
        testData.update();
        eLogger.errorOT(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("errorOT", EventLevel.ERROR,
                            EventType.OPERATIONAL, EventSensitivity.TS,
                            testData);
        testData.update();
        eLogger.errorOU(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("errorOU", EventLevel.ERROR,
                            EventType.OPERATIONAL,
                            EventSensitivity.UNCLASSIFIED, testData);
        testData.update();
        eLogger.errorSC(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("errorSC", EventLevel.ERROR,
                            EventType.SECURITY,
                            EventSensitivity.CONFIDENTIAL, testData);
        testData.update();
        eLogger.errorSF(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("errorSF", EventLevel.ERROR,
                            EventType.SECURITY, EventSensitivity.FOUO,
                            testData);
        testData.update();
        eLogger.errorSS(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("errorSS", EventLevel.ERROR,
                            EventType.SECURITY, EventSensitivity.SECRET,
                            testData);
        testData.update();
        eLogger.errorST(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("errorST", EventLevel.ERROR,
                            EventType.SECURITY, EventSensitivity.TS,
                            testData);
        testData.update();
        eLogger.errorSU(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("errorSU", EventLevel.ERROR,
                            EventType.SECURITY,
                            EventSensitivity.UNCLASSIFIED, testData);

        testData.update();
        testData.corId = NA;
        eLogger.metric(EventType.SECURITY, EventSensitivity.CONFIDENTIAL,
                       testData.msg, testData.params);
        eLogger.assertEvent("metric", EventLevel.METRIC,
                            EventType.SECURITY,
                            EventSensitivity.CONFIDENTIAL, testData);
        testData.update();
        testData.corId = NA;
        eLogger.metricAF(testData.msg, testData.params);
        eLogger.assertEvent("metricAF", EventLevel.METRIC,
                            EventType.APPLICATION, EventSensitivity.FOUO,
                            testData);
        testData.update();
        testData.corId = NA;
        eLogger.metricBC(testData.msg, testData.params);
        eLogger.assertEvent("metricBC", EventLevel.METRIC,
                            EventType.BUSINESS,
                            EventSensitivity.CONFIDENTIAL, testData);

        testData.update();
        eLogger.panic(EventType.SECURITY, EventSensitivity.CONFIDENTIAL,
                      testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("panic", EventLevel.PANIC, EventType.SECURITY,
                            EventSensitivity.CONFIDENTIAL, testData);
        testData.update();
        eLogger.panicAF(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("panicAF", EventLevel.PANIC,
                            EventType.APPLICATION, EventSensitivity.FOUO,
                            testData);
        testData.update();
        eLogger.panicBC(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("panicBC", EventLevel.PANIC,
                            EventType.BUSINESS,
                            EventSensitivity.CONFIDENTIAL, testData);
        testData.update();
        eLogger.panicOF(testData.corId, testData.msg, testData.params);
        eLogger.assertEvent("panicOF", EventLevel.PANIC,
                            EventType.OPERATIONAL, EventSensitivity.FOUO,
                            testData);

    }

    @Test
    void testOriginalLogger() {
        final var logger = Logger.getLogger("TestO");
        logger.info("Test");
        logger.log(Level.INFO, "Test {0}", "TestParam");
        Assertions.assertNotNull(logger);
    }
}
