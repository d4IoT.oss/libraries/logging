package com.d4iot.oss.corelib.logging.logger.test;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.LogRecord;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.d4iot.oss.corelib.logging.logger.EventLevel;
import com.d4iot.oss.corelib.logging.logger.EventLogger;
import com.d4iot.oss.corelib.logging.logger.EventManager;
import com.d4iot.oss.corelib.logging.logger.EventRecord;
import com.d4iot.oss.corelib.logging.logger.EventSensitivity;
import com.d4iot.oss.corelib.logging.logger.EventType;
import com.d4iot.oss.corelib.logging.logger.event.EventRecordProto.Event;
import com.google.protobuf.InvalidProtocolBufferException;

class EventRecordTest {

    @Test
    void testlevelMapping() {
        Assertions.assertEquals(EventLevel.CONFIG,
                                EventLevel.getEventLevel(Level.CONFIG));
        Assertions.assertEquals(EventLevel.DEBUG,
                                EventLevel.getEventLevel(Level.FINE));
        Assertions.assertEquals(EventLevel.ERROR,
                                EventLevel.getEventLevel(Level.SEVERE));
        Assertions.assertEquals(EventLevel.INFO,
                                EventLevel.getEventLevel(Level.INFO));
        Assertions.assertEquals(EventLevel.PANIC,
                                EventLevel.getEventLevel(Level.OFF));
        Assertions.assertEquals(EventLevel.TRACE,
                                EventLevel.getEventLevel(Level.FINEST));
        Assertions.assertEquals(EventLevel.UNKNOWN_LEVEL,
                                EventLevel.getEventLevel(Level.ALL));
        Assertions.assertEquals(EventLevel.WARNING,
                                EventLevel.getEventLevel(Level.WARNING));

        Assertions.assertEquals(EventLevel.CONFIG, EventLevel
                .getEventLevel(new EventRecord.EventLevelClass("NA",
                        Level.CONFIG.intValue() + 99)));
        Assertions.assertEquals(EventLevel.DEBUG, EventLevel
                .getEventLevel(new EventRecord.EventLevelClass("NA",
                        Level.FINE.intValue() + 99)));
        Assertions.assertEquals(EventLevel.ERROR, EventLevel
                .getEventLevel(new EventRecord.EventLevelClass("NA",
                        Level.SEVERE.intValue() + 99)));
        Assertions.assertEquals(EventLevel.INFO, EventLevel
                .getEventLevel(new EventRecord.EventLevelClass("NA",
                        Level.INFO.intValue() + 99)));
        Assertions.assertEquals(EventLevel.TRACE, EventLevel
                .getEventLevel(new EventRecord.EventLevelClass("NA",
                        Level.FINEST.intValue() + 99)));
        Assertions.assertEquals(EventLevel.WARNING, EventLevel
                .getEventLevel(new EventRecord.EventLevelClass("NA",
                        Level.WARNING.intValue() + 99)));

        Assertions.assertNotNull(EventLevel.CONFIG.shortName);
        Assertions.assertNotNull(EventLevel.DEBUG.shortName);
        Assertions.assertNotNull(EventLevel.ERROR.shortName);
        Assertions.assertNotNull(EventLevel.INFO.shortName);
        Assertions.assertNotNull(EventLevel.METRIC.shortName);
        Assertions.assertNotNull(EventLevel.PANIC.shortName);
        Assertions.assertNotNull(EventLevel.TRACE.shortName);
        Assertions.assertNotNull(EventLevel.UNKNOWN_LEVEL.shortName);

        Assertions.assertNotNull(EventType.APPLICATION.shortName);
        Assertions.assertNotNull(EventType.BUSINESS.shortName);
        Assertions.assertNotNull(EventType.DATA.shortName);
        Assertions.assertNotNull(EventType.OPERATIONAL.shortName);
        Assertions.assertNotNull(EventType.SECURITY.shortName);
        Assertions.assertNotNull(EventType.UNKNOWN_TYPE.shortName);

        Assertions.assertNotNull(EventSensitivity.CONFIDENTIAL.shortName);
        Assertions.assertNotNull(EventSensitivity.FOUO.shortName);
        Assertions.assertNotNull(EventSensitivity.SECRET.shortName);
        Assertions.assertNotNull(EventSensitivity.SECRET_L1.shortName);
        Assertions.assertNotNull(EventSensitivity.SECRET_L2.shortName);
        Assertions.assertNotNull(EventSensitivity.SECRET_L3.shortName);
        Assertions.assertNotNull(EventSensitivity.SECRET_L4.shortName);
        Assertions.assertNotNull(EventSensitivity.SECRET_L5.shortName);
        Assertions.assertNotNull(EventSensitivity.TS.shortName);
        Assertions.assertNotNull(EventSensitivity.TS_L1.shortName);
        Assertions.assertNotNull(EventSensitivity.TS_L2.shortName);
        Assertions.assertNotNull(EventSensitivity.TS_L3.shortName);
        Assertions.assertNotNull(EventSensitivity.TS_L4.shortName);
        Assertions.assertNotNull(EventSensitivity.TS_L5.shortName);
        Assertions.assertNotNull(EventSensitivity.UNCLASSIFIED.shortName);
        Assertions
                .assertNotNull(EventSensitivity.UNKNOWN_SENSITIVITY.shortName);

    }

    @Test
    void testLogRecordConstructor() {
        final var logger = EventLogger.getLogger("TestTLRC", null);
        // Create/Prepare a log record
        final var lRec = new LogRecord(Level.SEVERE, "This is  a test");
        final List<String> paramElems = new ArrayList<>();
        paramElems.add("a");
        paramElems.add("b");
        lRec.setParameters(paramElems.toArray());
        lRec.setThrown(new RuntimeException("TestException"));
        lRec.setThreadID(999);
        lRec.setInstant(Instant.ofEpochMilli(0));
        lRec.setLoggerName(logger.getName());
        // Create event record from LogRecord
        final var eRec = new EventRecord(logger, lRec);
        Assertions.assertNotNull(eRec);

        // Validate LogRec portion of eRec
        Assertions.assertArrayEquals(lRec.getParameters(),
                                     eRec.getParameters());
        Assertions.assertEquals(lRec.getLevel().intValue(),
                                eRec.getLevel().intValue());
        Assertions.assertEquals(lRec.getInstant(), eRec.getInstant());
        Assertions.assertEquals(lRec.getMessage(), eRec.getMessage());
        Assertions.assertEquals(lRec.getMillis(), eRec.getMillis());
        Assertions.assertEquals(lRec.getResourceBundleName(),
                                eRec.getResourceBundleName());
        Assertions.assertEquals(lRec.getSequenceNumber(),
                                eRec.getSequenceNumber());
        Assertions.assertEquals(lRec.getThreadID(), eRec.getThreadID());
        Assertions.assertEquals(lRec.getThrown(), eRec.getThrown());
        Assertions.assertEquals(lRec.getLoggerName(),
                                eRec.getLoggerName());

        // Validate eRec
        Assertions.assertEquals(logger.getShortName(),
                                eRec.getLoggerShortName());
        Assertions.assertEquals(
                                EventManager.getId()
                                        .concat(logger.getLoaderName()),
                                eRec.getLogSource());
        Assertions.assertEquals(EventType.OPERATIONAL,
                                eRec.getEventType());
        Assertions.assertEquals(logger.getDefaultApplicationSensitivty(),
                                eRec.getEventSensitivity());
        Assertions.assertEquals(EventLevel.ERROR, eRec.getSeverityLevel());
        Assertions.assertNull(eRec.getCorrelationId());
        Assertions.assertEquals(lRec.getResourceBundleName(),
                                eRec.getResourceReference());
        Assertions.assertNull(eRec.getResourceBundle());
        Assertions.assertNull(eRec.getSourceClassName());
        Assertions.assertNull(eRec.getSourceMethodName());
        Assertions.assertArrayEquals(paramElems.toArray(),
                                     eRec.getMessageParams().toArray());

        // Pesky deprecated ones we need to test
        eRec.setSourceClassName("TestClass");
        Assertions.assertEquals("TestClass", eRec.getSourceClassName());
        eRec.setSourceMethodName("TestMethod");
        Assertions.assertEquals("TestMethod", eRec.getSourceMethodName());
        eRec.setThrown(null);
        Assertions.assertNull(eRec.getThrown());
        eRec.setResourceBundleName(null);
        eRec.setResourceBundle(null);
        eRec.setMessage("NewMsg");
        Assertions.assertEquals("NewMsg", eRec.getMessage());
        eRec.setParameters(null);
        Assertions.assertTrue(eRec.getMessageParams().isEmpty());
        eRec.setParameters(new Object[0]);
        Assertions.assertArrayEquals(new Object[0],
                                     eRec.getMessageParams().toArray());

        eRec.setParameters(paramElems.toArray());
        Assertions.assertArrayEquals(paramElems.toArray(),
                                     eRec.getMessageParams().toArray());

        Assertions.assertThrows(IncompatibleClassChangeError.class,
                                () -> eRec.setLevel(Level.SEVERE));
    }

    @Test
    void testProto() {
        final var logger = EventLogger.getLogger("TestPROTO", null);
        final var eRec = new EventRecord(logger, EventLevel.WARNING,
                EventType.APPLICATION, EventSensitivity.CONFIDENTIAL,
                "TestCorId", "Param");
        Assertions.assertNotNull(eRec);
        final var ev = eRec.getProtoData();
        Assertions.assertEquals(eRec.getMessage(), ev.getMessage());
        Assertions.assertArrayEquals(eRec.getMessageParams().toArray(),
                                     ev.getMessageParamsList().toArray());
        Assertions.assertEquals("", ev.getExceptionTrace());
        Assertions.assertEquals("", ev.getResourceRef());
    }

    @Test
    void testProtoMisc() throws InvalidProtocolBufferException {
        final var logger =
                EventLogger.getLogger("TestProtoMisc", "testProps");
        final var eRec = new EventRecord(logger, EventLevel.WARNING,
                EventType.APPLICATION, EventSensitivity.CONFIDENTIAL,
                "TestCorId", "ParamAVal %s", "ParamA",
                new RuntimeException("TestProto"));
        Assertions.assertNotNull(eRec);
        final var evIn = eRec.getProtoData();
        Assertions.assertArrayEquals(evIn.toByteString().toByteArray(),
                                     evIn.toByteArray());

        final var ev = Event.parseFrom(evIn.toByteString());

        Assertions.assertEquals(eRec.getCorrelationId(),
                                ev.getCorrelationId());
        Assertions.assertEquals(eRec.getSeverityLevel().id,
                                ev.getErrorLevelValue());
        Assertions.assertEquals(eRec.getEventSensitivity().id,
                                ev.getEventSensitivityValue());
        Assertions.assertEquals(eRec.getLogSource(), ev.getEventSource());
        Assertions.assertEquals(eRec.getEventType().id,
                                ev.getEventTypeValue());
        Assertions.assertEquals(eRec.getExceptionTrace(),
                                ev.getExceptionTrace());
        Assertions.assertEquals(0, ev.getIdentifier());
        Assertions.assertEquals(eRec.getMessage(), ev.getMessage());
        Assertions.assertArrayEquals(eRec.getMessageParams().toArray(),
                                     ev.getMessageParamsList().toArray());
        Assertions.assertEquals(eRec.getThreadID(), ev.getPseudoId());
        Assertions.assertEquals("testProps", ev.getResourceRef());
        Assertions.assertEquals(eRec.getSequenceNumber(),
                                ev.getSequence());
        Assertions.assertEquals(eRec.getMillis(), ev.getTime());
    }

    @Test
    void testProtoParams() {
        final var logger = EventLogger.getLogger("TestProtoParams", null);
        final var eRec = new EventRecord(logger, EventLevel.WARNING,
                EventType.BUSINESS, EventSensitivity.CONFIDENTIAL,
                "TestCorId", "ParamVal %s", "ParamA");
        Assertions.assertNotNull(eRec);
        final var ev = eRec.getProtoData();
        Assertions.assertEquals(eRec.getMessage(), ev.getMessage());
        Assertions.assertArrayEquals(eRec.getMessageParams().toArray(),
                                     ev.getMessageParamsList().toArray());
        Assertions.assertEquals("", ev.getExceptionTrace());
        Assertions.assertEquals("", ev.getResourceRef());
    }

    @Test
    void testProtoParamsWithExc() {
        final var logger =
                EventLogger.getLogger("TestProtoParamsWE", "testProps");
        final var eRec = new EventRecord(logger, EventLevel.WARNING,
                EventType.APPLICATION, EventSensitivity.CONFIDENTIAL,
                "TestCorId", "ParamAVal %s", "ParamA",
                new RuntimeException("TestProto"));
        Assertions.assertNotNull(eRec);
        final var ev = eRec.getProtoData();
        Assertions.assertEquals(eRec.getCorrelationId(),
                                ev.getCorrelationId());
        Assertions.assertEquals(eRec.getSeverityLevel().id,
                                ev.getErrorLevelValue());
        Assertions.assertEquals(eRec.getEventSensitivity().id,
                                ev.getEventSensitivityValue());
        Assertions.assertEquals(eRec.getLogSource(), ev.getEventSource());
        Assertions.assertEquals(eRec.getEventType().id,
                                ev.getEventTypeValue());
        Assertions.assertEquals(eRec.getExceptionTrace(),
                                ev.getExceptionTrace());
        Assertions.assertEquals(0, ev.getIdentifier());
        Assertions.assertEquals(eRec.getMessage(), ev.getMessage());
        Assertions.assertArrayEquals(eRec.getMessageParams().toArray(),
                                     ev.getMessageParamsList().toArray());
        Assertions.assertEquals(eRec.getThreadID(), ev.getPseudoId());
        Assertions.assertEquals("testProps", ev.getResourceRef());
        Assertions.assertEquals(eRec.getSequenceNumber(),
                                ev.getSequence());
        Assertions.assertEquals(eRec.getMillis(), ev.getTime());
    }

    @Test
    void testProtoParamsWithExcNoElems() {
        final var logger = EventLogger.getLogger("TestPPWENE", null);
        final Exception exc = new RuntimeException("TestE1E");
        exc.setStackTrace(new StackTraceElement[0]);
        final var eRec = new EventRecord(logger, EventLevel.WARNING,
                EventType.APPLICATION, EventSensitivity.CONFIDENTIAL,
                "TestCorId", "ParamAVal %s", "ParamA", exc);
        Assertions.assertNotNull(eRec);
        final var ev = eRec.getProtoData();
        Assertions.assertEquals(eRec.getExceptionTrace(),
                                ev.getExceptionTrace());
    }

}
