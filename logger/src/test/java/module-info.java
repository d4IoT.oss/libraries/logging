open module com.d4iot.oss.corelib.logging.logger.test {

    requires java.management;
    requires java.logging;
    requires com.google.protobuf;

    requires transitive com.d4iot.oss.corelib.logging.logger;

    requires transitive org.junit.jupiter.engine;
    requires transitive org.junit.jupiter.api;
    requires rest.assured.common;
    requires org.hamcrest;

}
