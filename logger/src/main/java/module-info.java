module com.d4iot.oss.corelib.logging.logger {
    requires java.management;
    requires transitive java.logging;
    requires transitive com.google.protobuf;

    exports com.d4iot.oss.corelib.logging.logger;
    exports com.d4iot.oss.corelib.logging.logger.event;

}
