// Copyright 2021 Lloyd Fernandes <lloyd-oss@astute.biz>
// SPDX-License-Identifier: GPL-3.0-or-later WITH Classpath-exception-2.0
// SPDX-License-Identifier: LicenseRef-d4iot-oss-derived-license
package com.d4iot.oss.corelib.logging.logger;

public enum EventSensitivity {
    UNKNOWN_SENSITIVITY(0, "X"), UNCLASSIFIED(5, "U"), FOUO(10, "F"),
    CONFIDENTIAL(20, "C"), SECRET(30, "S"), SECRET_L1(31, "S1"),
    SECRET_L2(32, "S2"), SECRET_L3(33, "S3"), SECRET_L4(34, "S4"),
    SECRET_L5(35, "S5"), TS(50, "T"), TS_L1(51, "T1"), TS_L2(52, "T2"),
    TS_L3(53, "T3"), TS_L4(54, "T4"), TS_L5(55, "T5");

    public final int id;
    public final String shortName;

    EventSensitivity(final int pId, final String pShortName) {
        id = pId;
        shortName = pShortName;
    }

}
