// Copyright 2021 Lloyd Fernandes <lloyd-oss@astute.biz>
// SPDX-License-Identifier: GPL-3.0-or-later WITH Classpath-exception-2.0
// SPDX-License-Identifier: LicenseRef-d4iot-oss-derived-license
package com.d4iot.oss.corelib.logging.logger.impl;

import java.util.logging.LogRecord;
import java.util.logging.Logger;

import com.d4iot.oss.corelib.logging.logger.EventLogger;
import com.d4iot.oss.corelib.logging.logger.EventRecord;

/**
 * The Class EventLogger.
 *
 * @author Lloyd Fernandes, d4IoT.com
 */
public class EventLoggerImpl extends Logger {

    /**
     * The Constant EMPTY_OBJ_ARRAY.
     */
    private static final Object[] EMPTY_OBJ_ARRAY = new Object[0];

    private final EventLogger evLogger;

    /**
     * Instantiates a new event logger.
     *
     * @param pName
     *            the name
     * @param pResourceBundleName
     *            the resource bundle name
     */
    public EventLoggerImpl(final String pName,
            final String pResourceBundleName,
            final EventLogger pEventLogger) {
        super(pName, pResourceBundleName);
        evLogger = pEventLogger;
    }

    public EventLogger getEventLogger() {
        return evLogger;
    }

    /**
     * {@inheritDoc}
     *
     * @see java.util.logging.Logger#log(java.util.logging.LogRecord)
     */
    @Override
    public void log(
            final LogRecord pRecord) {
        if (!(pRecord instanceof EventRecord)) {
            if (pRecord.getParameters() == null) {
                pRecord.setParameters(EMPTY_OBJ_ARRAY);
            }
            super.log(new EventRecord(evLogger, pRecord));
        } else {
            super.log(pRecord);
        }
    }

}
