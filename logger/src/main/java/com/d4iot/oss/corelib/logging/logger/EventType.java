// Copyright 2021 Lloyd Fernandes <lloyd-oss@astute.biz>
// SPDX-License-Identifier: GPL-3.0-or-later WITH Classpath-exception-2.0
// SPDX-License-Identifier: LicenseRef-d4iot-oss-derived-license
package com.d4iot.oss.corelib.logging.logger;

public enum EventType {
    UNKNOWN_TYPE(0, "X"), OPERATIONAL(1, "O"), SECURITY(5, "S"),
    APPLICATION(10, "A"), BUSINESS(20, "B"), DATA(25, "D");

    public final int id;
    public final String shortName;

    EventType(final int pId, final String pShortName) {
        id = pId;
        shortName = pShortName;
    }

}
