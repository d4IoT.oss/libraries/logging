// Copyright 2021 Lloyd Fernandes <lloyd-oss@astute.biz>
// SPDX-License-Identifier: GPL-3.0-or-later WITH Classpath-exception-2.0
// SPDX-License-Identifier: LicenseRef-d4iot-oss-derived-license
package com.d4iot.oss.corelib.logging.logger;

import java.util.logging.Logger;

import com.d4iot.oss.corelib.logging.logger.impl.EventLoggerImpl;

/**
 * The Class EventLogger.
 *
 * @author Lloyd Fernandes, d4IoT.com
 */
public class EventLogger {

    /**
     * The default event type.
     */
    private static EventType defaultEventType = EventType.APPLICATION;

    /**
     * The default event sensitivity.
     */
    private static EventSensitivity defaultEventSensitivity =
            EventSensitivity.FOUO;

    /**
     * The default business event sensitivity.
     */
    private static EventSensitivity defaultBusinessEventSensitivity =
            EventSensitivity.CONFIDENTIAL;

    /**
     * The Constant NA.
     */
    public static final String NA = "NA";

    /**
     * The default business sensitivty.
     */
    private EventSensitivity defaultBusinessSensitivty =
            defaultBusinessEventSensitivity;

    /**
     * The default operational sensitivty.
     */
    private EventSensitivity defaultOperationalSensitivty =
            defaultEventSensitivity;

    /**
     * The default application sensitivty.
     */
    private EventSensitivity defaultApplicationSensitivty =
            defaultEventSensitivity;

    private final EventLoggerImpl eventLoggerDelegate;
    /**
     * The loader name.
     */
    private final String loaderName;

    private final String shortName;

    protected EventLogger(final String pName,
            final String pResourceBundleName,
            final EventSensitivity pDefaultOperationalSensitivity,
            final EventSensitivity pDefaultAppSensitivity,
            final EventSensitivity pDefaultBusSensitivity) {
        eventLoggerDelegate =
                new EventLoggerImpl(pName, pResourceBundleName, this);
        setDefaultOperationalSensitivty(pDefaultOperationalSensitivity);
        setDefaultApplicationSensitivty(pDefaultAppSensitivity);
        setDefaultBusinessSensitivty(pDefaultBusSensitivity);
        loaderName = pName.concat("^")
                .concat(this.getClass().getClassLoader().getName());
        final var sb = new StringBuilder();
        final var toks = pName.split("\\.");
        final var len = toks.length;
        for (var idx = 0; idx < (len - 2); idx++) {
            sb.append(toks[idx].charAt(0)).append('.');
        }
        if (len > 1) {
            sb.append(toks[len - 2]).append('.');
        }
        sb.append(toks[len - 1]);
        shortName = sb.toString();

    }

    EventLogger(final Logger pLogger) {
        this(pLogger.getName(), pLogger.getResourceBundleName(),
                defaultEventSensitivity, defaultEventSensitivity,
                defaultBusinessEventSensitivity);
    }

    /**
     * Config.
     *
     * @param pType
     *            the type
     * @param pSensitivty
     *            the sensitivty
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void config(
            final EventType pType,
            final EventSensitivity pSensitivty,
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.CONFIG, pType, pSensitivty, pCorrelationId,
                 pMsg, pMsgFormatValues);
    }

    /**
     * Config AC.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void configAC(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.CONFIG, EventType.APPLICATION,
                 EventSensitivity.CONFIDENTIAL, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Config AF.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void configAF(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.CONFIG, EventType.APPLICATION,
                 EventSensitivity.FOUO, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Config AS.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void configAS(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.CONFIG, EventType.APPLICATION,
                 EventSensitivity.SECRET, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Config AT.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void configAT(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.CONFIG, EventType.APPLICATION,
                 EventSensitivity.TS, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Config AU.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void configAU(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.CONFIG, EventType.APPLICATION,
                 EventSensitivity.UNCLASSIFIED, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Config BC.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void configBC(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.CONFIG, EventType.BUSINESS,
                 EventSensitivity.CONFIDENTIAL, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Config BF.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void configBF(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.CONFIG, EventType.BUSINESS,
                 EventSensitivity.FOUO, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Config BS.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void configBS(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.CONFIG, EventType.BUSINESS,
                 EventSensitivity.SECRET, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Config BT.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void configBT(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.CONFIG, EventType.BUSINESS,
                 EventSensitivity.TS, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Config BU.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void configBU(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.CONFIG, EventType.BUSINESS,
                 EventSensitivity.UNCLASSIFIED, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Config DC.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void configDC(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.CONFIG, EventType.DATA,
                 EventSensitivity.CONFIDENTIAL, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Config DF.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void configDF(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.CONFIG, EventType.DATA, EventSensitivity.FOUO,
                 pCorrelationId, pMsg, pMsgFormatValues);
    }

    /**
     * Config DS.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void configDS(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.CONFIG, EventType.DATA,
                 EventSensitivity.SECRET, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Config DT.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void configDT(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.CONFIG, EventType.DATA, EventSensitivity.TS,
                 pCorrelationId, pMsg, pMsgFormatValues);
    }

    /**
     * Config DU.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void configDU(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.CONFIG, EventType.DATA,
                 EventSensitivity.UNCLASSIFIED, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Config OC.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void configOC(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.CONFIG, EventType.OPERATIONAL,
                 EventSensitivity.CONFIDENTIAL, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Config OF.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void configOF(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.CONFIG, EventType.OPERATIONAL,
                 EventSensitivity.FOUO, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Config OS.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void configOS(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.CONFIG, EventType.OPERATIONAL,
                 EventSensitivity.SECRET, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Config OT.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void configOT(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.CONFIG, EventType.OPERATIONAL,
                 EventSensitivity.TS, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Config OU.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void configOU(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.CONFIG, EventType.OPERATIONAL,
                 EventSensitivity.UNCLASSIFIED, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Config SC.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void configSC(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.CONFIG, EventType.SECURITY,
                 EventSensitivity.CONFIDENTIAL, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Config SF.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void configSF(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.CONFIG, EventType.SECURITY,
                 EventSensitivity.FOUO, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Config SS.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void configSS(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.CONFIG, EventType.SECURITY,
                 EventSensitivity.SECRET, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Config ST.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void configST(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.CONFIG, EventType.SECURITY,
                 EventSensitivity.TS, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Config SU.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void configSU(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.CONFIG, EventType.SECURITY,
                 EventSensitivity.UNCLASSIFIED, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Debug.
     *
     * @param pType
     *            the type
     * @param pSensitivty
     *            the sensitivty
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void debug(
            final EventType pType,
            final EventSensitivity pSensitivty,
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.DEBUG, pType, pSensitivty, pCorrelationId,
                 pMsg, pMsgFormatValues);
    }

    /**
     * Debug AC.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void debugAC(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.DEBUG, EventType.APPLICATION,
                 EventSensitivity.CONFIDENTIAL, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Debug AF.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void debugAF(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.DEBUG, EventType.APPLICATION,
                 EventSensitivity.FOUO, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Debug AS.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void debugAS(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.DEBUG, EventType.APPLICATION,
                 EventSensitivity.SECRET, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Debug AT.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void debugAT(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.DEBUG, EventType.APPLICATION,
                 EventSensitivity.TS, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Debug AU.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void debugAU(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.DEBUG, EventType.APPLICATION,
                 EventSensitivity.UNCLASSIFIED, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Debug BC.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void debugBC(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.DEBUG, EventType.BUSINESS,
                 EventSensitivity.CONFIDENTIAL, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Debug BF.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void debugBF(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.DEBUG, EventType.BUSINESS,
                 EventSensitivity.FOUO, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Debug BS.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void debugBS(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.DEBUG, EventType.BUSINESS,
                 EventSensitivity.SECRET, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Debug BT.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void debugBT(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.DEBUG, EventType.BUSINESS, EventSensitivity.TS,
                 pCorrelationId, pMsg, pMsgFormatValues);
    }

    /**
     * Debug BU.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void debugBU(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.DEBUG, EventType.BUSINESS,
                 EventSensitivity.UNCLASSIFIED, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Debug DC.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void debugDC(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.DEBUG, EventType.DATA,
                 EventSensitivity.CONFIDENTIAL, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Debug DF.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void debugDF(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.DEBUG, EventType.DATA, EventSensitivity.FOUO,
                 pCorrelationId, pMsg, pMsgFormatValues);
    }

    /**
     * Debug DS.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void debugDS(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.DEBUG, EventType.DATA, EventSensitivity.SECRET,
                 pCorrelationId, pMsg, pMsgFormatValues);
    }

    /**
     * Debug DT.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void debugDT(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.DEBUG, EventType.DATA, EventSensitivity.TS,
                 pCorrelationId, pMsg, pMsgFormatValues);
    }

    /**
     * Debug DU.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void debugDU(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.DEBUG, EventType.DATA,
                 EventSensitivity.UNCLASSIFIED, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Debug OC.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void debugOC(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.DEBUG, EventType.OPERATIONAL,
                 EventSensitivity.CONFIDENTIAL, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Debug OF.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void debugOF(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.DEBUG, EventType.OPERATIONAL,
                 EventSensitivity.FOUO, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Debug OS.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void debugOS(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.DEBUG, EventType.OPERATIONAL,
                 EventSensitivity.SECRET, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Debug OT.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void debugOT(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.DEBUG, EventType.OPERATIONAL,
                 EventSensitivity.TS, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Debug OU.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void debugOU(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.DEBUG, EventType.OPERATIONAL,
                 EventSensitivity.UNCLASSIFIED, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Debug SC.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void debugSC(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.DEBUG, EventType.SECURITY,
                 EventSensitivity.CONFIDENTIAL, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Debug SF.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void debugSF(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.DEBUG, EventType.SECURITY,
                 EventSensitivity.FOUO, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Debug SS.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void debugSS(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.DEBUG, EventType.SECURITY,
                 EventSensitivity.SECRET, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Debug ST.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void debugST(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.DEBUG, EventType.SECURITY, EventSensitivity.TS,
                 pCorrelationId, pMsg, pMsgFormatValues);
    }

    /**
     * Debug SU.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void debugSU(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.DEBUG, EventType.SECURITY,
                 EventSensitivity.UNCLASSIFIED, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Error.
     *
     * @param pType
     *            the type
     * @param pSensitivty
     *            the sensitivty
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void error(
            final EventType pType,
            final EventSensitivity pSensitivty,
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.ERROR, pType, pSensitivty, pCorrelationId,
                 pMsg, pMsgFormatValues);
    }

    /**
     * Error AC.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void errorAC(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.ERROR, EventType.APPLICATION,
                 EventSensitivity.CONFIDENTIAL, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Error AF.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void errorAF(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.ERROR, EventType.APPLICATION,
                 EventSensitivity.FOUO, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Error AS.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void errorAS(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.ERROR, EventType.APPLICATION,
                 EventSensitivity.SECRET, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Error AT.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void errorAT(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.ERROR, EventType.APPLICATION,
                 EventSensitivity.TS, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Error AU.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void errorAU(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.ERROR, EventType.APPLICATION,
                 EventSensitivity.UNCLASSIFIED, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Error BC.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void errorBC(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.ERROR, EventType.BUSINESS,
                 EventSensitivity.CONFIDENTIAL, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Error BF.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void errorBF(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.ERROR, EventType.BUSINESS,
                 EventSensitivity.FOUO, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Error BS.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void errorBS(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.ERROR, EventType.BUSINESS,
                 EventSensitivity.SECRET, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Error BT.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void errorBT(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.ERROR, EventType.BUSINESS, EventSensitivity.TS,
                 pCorrelationId, pMsg, pMsgFormatValues);
    }

    /**
     * Error BU.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void errorBU(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.ERROR, EventType.BUSINESS,
                 EventSensitivity.UNCLASSIFIED, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Error DC.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void errorDC(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.ERROR, EventType.DATA,
                 EventSensitivity.CONFIDENTIAL, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Error DF.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void errorDF(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.ERROR, EventType.DATA, EventSensitivity.FOUO,
                 pCorrelationId, pMsg, pMsgFormatValues);
    }

    /**
     * Error DS.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void errorDS(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.ERROR, EventType.DATA, EventSensitivity.SECRET,
                 pCorrelationId, pMsg, pMsgFormatValues);
    }

    /**
     * Error DT.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void errorDT(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.ERROR, EventType.DATA, EventSensitivity.TS,
                 pCorrelationId, pMsg, pMsgFormatValues);
    }

    /**
     * Error DU.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void errorDU(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.ERROR, EventType.DATA,
                 EventSensitivity.UNCLASSIFIED, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Error OC.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void errorOC(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.ERROR, EventType.OPERATIONAL,
                 EventSensitivity.CONFIDENTIAL, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Error OF.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void errorOF(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.ERROR, EventType.OPERATIONAL,
                 EventSensitivity.FOUO, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Error OS.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void errorOS(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.ERROR, EventType.OPERATIONAL,
                 EventSensitivity.SECRET, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Error OT.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void errorOT(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.ERROR, EventType.OPERATIONAL,
                 EventSensitivity.TS, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Error OU.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void errorOU(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.ERROR, EventType.OPERATIONAL,
                 EventSensitivity.UNCLASSIFIED, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Error SC.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void errorSC(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.ERROR, EventType.SECURITY,
                 EventSensitivity.CONFIDENTIAL, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Error SF.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void errorSF(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.ERROR, EventType.SECURITY,
                 EventSensitivity.FOUO, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Error SS.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void errorSS(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.ERROR, EventType.SECURITY,
                 EventSensitivity.SECRET, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Error ST.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void errorST(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.ERROR, EventType.SECURITY, EventSensitivity.TS,
                 pCorrelationId, pMsg, pMsgFormatValues);
    }

    /**
     * Error SU.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void errorSU(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.ERROR, EventType.SECURITY,
                 EventSensitivity.UNCLASSIFIED, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Gets the default application sensitivty.
     *
     * @return the default application sensitivty
     */
    public EventSensitivity getDefaultApplicationSensitivty() {
        return defaultApplicationSensitivty;
    }

    /**
     * Gets the default business sensitivty.
     *
     * @return the default business sensitivty
     */
    public EventSensitivity getDefaultBusinessSensitivty() {
        return defaultBusinessSensitivty;
    }

    /**
     * Gets the default operational sensitivty.
     *
     * @return the default operational sensitivty
     */
    public EventSensitivity getDefaultOperationalSensitivty() {
        return defaultOperationalSensitivty;
    }

    /**
     * Gets the loader name.
     *
     * @return the loader name
     */
    public String getLoaderName() {
        return loaderName;
    }

    public String getName() {
        return eventLoggerDelegate.getName();
    }

    public String getShortName() {
        return shortName;
    }

    /**
     * Info.
     *
     * @param pType
     *            the type
     * @param pSensitivty
     *            the sensitivty
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void info(
            final EventType pType,
            final EventSensitivity pSensitivty,
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.INFO, pType, pSensitivty, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Info AC.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void infoAC(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.INFO, EventType.APPLICATION,
                 EventSensitivity.CONFIDENTIAL, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Info AF.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void infoAF(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.INFO, EventType.APPLICATION,
                 EventSensitivity.FOUO, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Info AS.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void infoAS(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.INFO, EventType.APPLICATION,
                 EventSensitivity.SECRET, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Info AT.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void infoAT(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.INFO, EventType.APPLICATION,
                 EventSensitivity.TS, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Info AU.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void infoAU(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.INFO, EventType.APPLICATION,
                 EventSensitivity.UNCLASSIFIED, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Info BC.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void infoBC(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.INFO, EventType.BUSINESS,
                 EventSensitivity.CONFIDENTIAL, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Info BF.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void infoBF(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.INFO, EventType.BUSINESS,
                 EventSensitivity.FOUO, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Info BS.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void infoBS(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.INFO, EventType.BUSINESS,
                 EventSensitivity.SECRET, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Info BT.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void infoBT(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.INFO, EventType.BUSINESS, EventSensitivity.TS,
                 pCorrelationId, pMsg, pMsgFormatValues);
    }

    /**
     * Info BU.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void infoBU(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.INFO, EventType.BUSINESS,
                 EventSensitivity.UNCLASSIFIED, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Info DC.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void infoDC(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.INFO, EventType.DATA,
                 EventSensitivity.CONFIDENTIAL, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Info DF.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void infoDF(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.INFO, EventType.DATA, EventSensitivity.FOUO,
                 pCorrelationId, pMsg, pMsgFormatValues);
    }

    /**
     * Info DS.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void infoDS(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.INFO, EventType.DATA, EventSensitivity.SECRET,
                 pCorrelationId, pMsg, pMsgFormatValues);
    }

    /**
     * Info DT.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void infoDT(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.INFO, EventType.DATA, EventSensitivity.TS,
                 pCorrelationId, pMsg, pMsgFormatValues);
    }

    /**
     * Info DU.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void infoDU(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.INFO, EventType.DATA,
                 EventSensitivity.UNCLASSIFIED, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Info OC.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void infoOC(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.INFO, EventType.OPERATIONAL,
                 EventSensitivity.CONFIDENTIAL, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Info OF.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void infoOF(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.INFO, EventType.OPERATIONAL,
                 EventSensitivity.FOUO, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Info OS.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void infoOS(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.INFO, EventType.OPERATIONAL,
                 EventSensitivity.SECRET, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Info OT.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void infoOT(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.INFO, EventType.OPERATIONAL,
                 EventSensitivity.TS, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Info OU.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void infoOU(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.INFO, EventType.OPERATIONAL,
                 EventSensitivity.UNCLASSIFIED, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Info SC.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void infoSC(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.INFO, EventType.SECURITY,
                 EventSensitivity.CONFIDENTIAL, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Info SF.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void infoSF(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.INFO, EventType.SECURITY,
                 EventSensitivity.FOUO, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Info SS.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void infoSS(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.INFO, EventType.SECURITY,
                 EventSensitivity.SECRET, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Info ST.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void infoST(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.INFO, EventType.SECURITY, EventSensitivity.TS,
                 pCorrelationId, pMsg, pMsgFormatValues);
    }

    /**
     * Info SU.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void infoSU(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.INFO, EventType.SECURITY,
                 EventSensitivity.UNCLASSIFIED, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Log event.
     *
     * @param pSeverityLevel
     *            the severity level
     * @param pType
     *            the type
     * @param pSensitivty
     *            the sensitivty
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void logEvent(
            final EventLevel pSeverityLevel,
            final EventType pType,
            final EventSensitivity pSensitivty,
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        if (eventLoggerDelegate.isLoggable(pSeverityLevel.level)) {
            eventLoggerDelegate.log(new EventRecord(this, pSeverityLevel,
                    pType, pSensitivty, pCorrelationId, pMsg,
                    pMsgFormatValues));
        }

    }

    /**
     * Metric.
     *
     * @param pType
     *            the type
     * @param pSensitivty
     *            the sensitivty
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void metric(
            final EventType pType,
            final EventSensitivity pSensitivty,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.METRIC, pType, pSensitivty, NA, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Metric AF.
     *
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void metricAF(
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.METRIC, EventType.APPLICATION,
                 EventSensitivity.FOUO, NA, pMsg, pMsgFormatValues);
    }

    /**
     * Metric BC.
     *
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void metricBC(
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.METRIC, EventType.BUSINESS,
                 EventSensitivity.CONFIDENTIAL, NA, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Panic.
     *
     * @param pType
     *            the type
     * @param pSensitivty
     *            the sensitivty
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void panic(
            final EventType pType,
            final EventSensitivity pSensitivty,
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.PANIC, pType, pSensitivty, pCorrelationId,
                 pMsg, pMsgFormatValues);
    }

    /**
     * Panic AF.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void panicAF(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.PANIC, EventType.APPLICATION,
                 EventSensitivity.FOUO, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Panic BC.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void panicBC(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.PANIC, EventType.BUSINESS,
                 EventSensitivity.CONFIDENTIAL, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Panic OF.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void panicOF(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.PANIC, EventType.OPERATIONAL,
                 EventSensitivity.FOUO, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Sets the default application sensitivty.
     *
     * @param pDefaultApplicationSensitivty
     *            the new default application sensitivty
     */
    public void setDefaultApplicationSensitivty(
            final EventSensitivity pDefaultApplicationSensitivty) {
        defaultApplicationSensitivty = pDefaultApplicationSensitivty;
    }

    /**
     * Sets the default business sensitivty.
     *
     * @param pDefaultBusinessSensitivty
     *            the new default business sensitivty
     */
    public void setDefaultBusinessSensitivty(
            final EventSensitivity pDefaultBusinessSensitivty) {
        defaultBusinessSensitivty = pDefaultBusinessSensitivty;
    }

    /**
     * Sets the default operational sensitivty.
     *
     * @param pDefaultOperationalSensitivty
     *            the new default operational sensitivty
     */
    public void setDefaultOperationalSensitivty(
            final EventSensitivity pDefaultOperationalSensitivty) {
        defaultOperationalSensitivty = pDefaultOperationalSensitivty;
    }

    /**
     * Trace.
     *
     * @param pType
     *            the type
     * @param pSensitivty
     *            the sensitivty
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void trace(
            final EventType pType,
            final EventSensitivity pSensitivty,
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.TRACE, pType, pSensitivty, pCorrelationId,
                 pMsg, pMsgFormatValues);
    }

    /**
     * Trace AC.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void traceAC(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.TRACE, EventType.APPLICATION,
                 EventSensitivity.CONFIDENTIAL, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Trace AF.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void traceAF(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.TRACE, EventType.APPLICATION,
                 EventSensitivity.FOUO, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Trace AS.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void traceAS(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.TRACE, EventType.APPLICATION,
                 EventSensitivity.SECRET, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Trace AT.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void traceAT(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.TRACE, EventType.APPLICATION,
                 EventSensitivity.TS, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Trace AU.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void traceAU(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.TRACE, EventType.APPLICATION,
                 EventSensitivity.UNCLASSIFIED, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Trace BC.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void traceBC(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.TRACE, EventType.BUSINESS,
                 EventSensitivity.CONFIDENTIAL, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Trace BF.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void traceBF(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.TRACE, EventType.BUSINESS,
                 EventSensitivity.FOUO, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Trace BS.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void traceBS(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.TRACE, EventType.BUSINESS,
                 EventSensitivity.SECRET, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Trace BT.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void traceBT(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.TRACE, EventType.BUSINESS, EventSensitivity.TS,
                 pCorrelationId, pMsg, pMsgFormatValues);
    }

    /**
     * Trace BU.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void traceBU(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.TRACE, EventType.BUSINESS,
                 EventSensitivity.UNCLASSIFIED, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Trace DC.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void traceDC(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.TRACE, EventType.DATA,
                 EventSensitivity.CONFIDENTIAL, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Trace DF.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void traceDF(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.TRACE, EventType.DATA, EventSensitivity.FOUO,
                 pCorrelationId, pMsg, pMsgFormatValues);
    }

    /**
     * Trace DS.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void traceDS(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.TRACE, EventType.DATA, EventSensitivity.SECRET,
                 pCorrelationId, pMsg, pMsgFormatValues);
    }

    /**
     * Trace DT.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void traceDT(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.TRACE, EventType.DATA, EventSensitivity.TS,
                 pCorrelationId, pMsg, pMsgFormatValues);
    }

    /**
     * Trace DU.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void traceDU(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.TRACE, EventType.DATA,
                 EventSensitivity.UNCLASSIFIED, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Trace OC.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void traceOC(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.TRACE, EventType.OPERATIONAL,
                 EventSensitivity.CONFIDENTIAL, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Trace OF.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void traceOF(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.TRACE, EventType.OPERATIONAL,
                 EventSensitivity.FOUO, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Trace OS.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void traceOS(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.TRACE, EventType.OPERATIONAL,
                 EventSensitivity.SECRET, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Trace OT.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void traceOT(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.TRACE, EventType.OPERATIONAL,
                 EventSensitivity.TS, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Trace OU.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void traceOU(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.TRACE, EventType.OPERATIONAL,
                 EventSensitivity.UNCLASSIFIED, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Trace SC.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void traceSC(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.TRACE, EventType.SECURITY,
                 EventSensitivity.CONFIDENTIAL, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Trace SF.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void traceSF(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.TRACE, EventType.SECURITY,
                 EventSensitivity.FOUO, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Trace SS.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void traceSS(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.TRACE, EventType.SECURITY,
                 EventSensitivity.SECRET, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Trace ST.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void traceST(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.TRACE, EventType.SECURITY, EventSensitivity.TS,
                 pCorrelationId, pMsg, pMsgFormatValues);
    }

    /**
     * Trace SU.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void traceSU(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.TRACE, EventType.SECURITY,
                 EventSensitivity.UNCLASSIFIED, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Warn.
     *
     * @param pType
     *            the type
     * @param pSensitivty
     *            the sensitivty
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void warn(
            final EventType pType,
            final EventSensitivity pSensitivty,
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.WARNING, pType, pSensitivty, pCorrelationId,
                 pMsg, pMsgFormatValues);
    }

    /**
     * Warn AC.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void warnAC(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.WARNING, EventType.APPLICATION,
                 EventSensitivity.CONFIDENTIAL, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Warn AF.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void warnAF(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.WARNING, EventType.APPLICATION,
                 EventSensitivity.FOUO, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Warn AS.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void warnAS(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.WARNING, EventType.APPLICATION,
                 EventSensitivity.SECRET, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Warn AT.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void warnAT(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.WARNING, EventType.APPLICATION,
                 EventSensitivity.TS, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Warn AU.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void warnAU(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.WARNING, EventType.APPLICATION,
                 EventSensitivity.UNCLASSIFIED, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Warn BC.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void warnBC(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.WARNING, EventType.BUSINESS,
                 EventSensitivity.CONFIDENTIAL, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Warn BF.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void warnBF(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.WARNING, EventType.BUSINESS,
                 EventSensitivity.FOUO, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Warn BS.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void warnBS(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.WARNING, EventType.BUSINESS,
                 EventSensitivity.SECRET, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Warn BT.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void warnBT(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.WARNING, EventType.BUSINESS,
                 EventSensitivity.TS, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Warn BU.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void warnBU(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.WARNING, EventType.BUSINESS,
                 EventSensitivity.UNCLASSIFIED, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Warn DC.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void warnDC(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.WARNING, EventType.DATA,
                 EventSensitivity.CONFIDENTIAL, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Warn DF.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void warnDF(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.WARNING, EventType.DATA, EventSensitivity.FOUO,
                 pCorrelationId, pMsg, pMsgFormatValues);
    }

    /**
     * Warn DS.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void warnDS(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.WARNING, EventType.DATA,
                 EventSensitivity.SECRET, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Warn DT.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void warnDT(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.WARNING, EventType.DATA, EventSensitivity.TS,
                 pCorrelationId, pMsg, pMsgFormatValues);
    }

    /**
     * Warn DU.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void warnDU(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.WARNING, EventType.DATA,
                 EventSensitivity.UNCLASSIFIED, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Warn OC.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void warnOC(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.WARNING, EventType.OPERATIONAL,
                 EventSensitivity.CONFIDENTIAL, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Warn OF.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void warnOF(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.WARNING, EventType.OPERATIONAL,
                 EventSensitivity.FOUO, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Warn OS.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void warnOS(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.WARNING, EventType.OPERATIONAL,
                 EventSensitivity.SECRET, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Warn OT.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void warnOT(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.WARNING, EventType.OPERATIONAL,
                 EventSensitivity.TS, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Warn OU.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void warnOU(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.WARNING, EventType.OPERATIONAL,
                 EventSensitivity.UNCLASSIFIED, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Warn SC.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void warnSC(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.WARNING, EventType.SECURITY,
                 EventSensitivity.CONFIDENTIAL, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Warn SF.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void warnSF(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.WARNING, EventType.SECURITY,
                 EventSensitivity.FOUO, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Warn SS.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void warnSS(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.WARNING, EventType.SECURITY,
                 EventSensitivity.SECRET, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Warn ST.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void warnST(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.WARNING, EventType.SECURITY,
                 EventSensitivity.TS, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    /**
     * Warn SU.
     *
     * @param pCorrelationId
     *            the correlation id
     * @param pMsg
     *            the msg
     * @param pMsgFormatValues
     *            the msg format values
     */
    public void warnSU(
            final String pCorrelationId,
            final String pMsg,
            final Object... pMsgFormatValues) {
        logEvent(EventLevel.WARNING, EventType.SECURITY,
                 EventSensitivity.UNCLASSIFIED, pCorrelationId, pMsg,
                 pMsgFormatValues);
    }

    EventLoggerImpl getImplDelegate() {
        return eventLoggerDelegate;
    }

    /**
     * Gets the default business event sensitivity.
     *
     * @return the default business event sensitivity
     */
    public static EventSensitivity getDefaultBusinessEventSensitivity() {
        return defaultBusinessEventSensitivity;
    }

    /**
     * Gets the default event sensitivity.
     *
     * @return the default event sensitivity
     */
    public static EventSensitivity getDefaultEventSensitivity() {
        return defaultEventSensitivity;
    }

    /**
     * Gets the default event type.
     *
     * @return the default event type
     */
    public static EventType getDefaultEventType() {
        return defaultEventType;
    }

    public static EventLogger getLogger(
            final String pName,
            final EventSensitivity pOperationalSensitivity,
            final EventSensitivity pAppSensitivity,
            final EventSensitivity pBusSensitivity) {
        return getLogger(pName, null, pOperationalSensitivity,
                         pAppSensitivity, pBusSensitivity);
    }

    public static EventLogger getLogger(
            final String pName,
            final String pResourceBundleName) {
        return getLogger(pName, pResourceBundleName,
                         defaultEventSensitivity, defaultEventSensitivity,
                         defaultBusinessEventSensitivity);
    }

    /**
     * Gets the logger.
     *
     * @param pName
     *            the name
     * @param pResourceBundleName
     *            the resource bundle name
     * @param pOperationalSensitivity
     *            the sys sensitivity
     * @param pAppSensitivity
     *            the app sensitivity
     * @param pBusSensitivity
     *            the bus sensitivity
     * @return the logger
     */
    public static EventLogger getLogger(
            final String pName,
            final String pResourceBundleName,
            final EventSensitivity pOperationalSensitivity,
            final EventSensitivity pAppSensitivity,
            final EventSensitivity pBusSensitivity) {
        final var logger = new EventLogger(pName, pResourceBundleName,
                pOperationalSensitivity, pAppSensitivity, pBusSensitivity);
        return EventManager.getEventManager().addLogger(logger);
    }

    /**
     * Sets the default business event sensitivity.
     *
     * @param pDefaultBusinessEventSensitivity
     *            the new default business event sensitivity
     */
    public static void setDefaultBusinessEventSensitivity(
            final EventSensitivity pDefaultBusinessEventSensitivity) {
        defaultBusinessEventSensitivity = pDefaultBusinessEventSensitivity;
    }

    /**
     * Sets the default event sensitivity.
     *
     * @param pDefaultEventSensitivity
     *            the new default event sensitivity
     */
    public static void setDefaultEventSensitivity(
            final EventSensitivity pDefaultEventSensitivity) {
        defaultEventSensitivity = pDefaultEventSensitivity;
    }

    /**
     * Sets the default event type.
     *
     * @param pDefaultEventType
     *            the new default event type
     */
    public static void setDefaultEventType(
            final EventType pDefaultEventType) {
        defaultEventType = pDefaultEventType;
    }

}
