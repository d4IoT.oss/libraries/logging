// Copyright 2021 Lloyd Fernandes <lloyd-oss@astute.biz>
// SPDX-License-Identifier: GPL-3.0-or-later WITH Classpath-exception-2.0
// SPDX-License-Identifier: LicenseRef-d4iot-oss-derived-license
package com.d4iot.oss.corelib.logging.logger;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.LogRecord;

import com.d4iot.oss.corelib.logging.logger.event.EventRecordProto;
import com.d4iot.oss.corelib.logging.logger.event.EventRecordProto.Event;

public class EventRecord extends LogRecord {

    public static final class EventLevelClass extends Level {

        private static final long serialVersionUID = -3288461024274356433L;

        public EventLevelClass(final String pName, final int pValue) {
            super(pName, pValue);
        }

    }

    private static final long serialVersionUID = 7799871925427146444L;

    private final EventType eventType;
    private final EventLevel eventLevel;
    private final EventSensitivity eventSensitivity;
    private final String correlationId;
    private final String logSource;
    private final String loggerShortName;
    private final List<String> messageParams = new ArrayList<>();

    private final String resourceReference;
    private Throwable thrown;

    public EventRecord(final EventLogger pLogger,
            final EventLevel pSeverityLevel, final EventType pType,
            final EventSensitivity pSensitivty,
            final String pCorrelationId, final String pMsg,
            final Object... pMsgFormatValues) {
        super(pSeverityLevel.level, pMsg);
        setSourceClassName(null); // suppress infer caller
        eventLevel = pSeverityLevel;
        logSource = EventManager.getId().concat(pLogger.getLoaderName());
        loggerShortName = pLogger.getShortName();
        eventType = pType;
        eventSensitivity = pSensitivty;
        correlationId = pCorrelationId;
        resourceReference =
                pLogger.getImplDelegate().getResourceBundleName();
        super.setResourceBundle(pLogger.getImplDelegate()
                .getResourceBundle());
        setLoggerName(pLogger.getImplDelegate().getName());

        if (pMsgFormatValues.length > 0) {
            var len = pMsgFormatValues.length;
            if ((eventType.id < EventType.BUSINESS.id)
                    && (pMsgFormatValues[len - 1] instanceof Throwable)) {
                len--;
                thrown = (Throwable) pMsgFormatValues[len];
            }

            for (var i = 0; i < len; i++) {
                messageParams.add(pMsgFormatValues[i].toString());
            }
        }
    }

    public EventRecord(final EventLogger pLogger,
            final LogRecord pRecord) {
        this(pLogger, EventLevel.getEventLevel(pRecord.getLevel()),
                EventType.OPERATIONAL,
                pLogger.getDefaultApplicationSensitivty(), null,
                pRecord.getMessage(), pRecord.getParameters());
        setSequenceNumber(pRecord.getSequenceNumber());
        setInstant(pRecord.getInstant());
        setThreadID(pRecord.getThreadID());
        thrown = pRecord.getThrown();
    }

    public String getCorrelationId() {
        return correlationId;
    }

    public EventSensitivity getEventSensitivity() {
        return eventSensitivity;
    }

    public EventType getEventType() {
        return eventType;
    }

    public String getExceptionTrace() {
        if (thrown == null) {
            return null;
        }
        final var stackElems = thrown.getStackTrace();
        final var stackMsg = new StringBuilder(1024);
        stackMsg.append(thrown.getMessage());
        if (stackElems.length > 0) {

            for (var i = stackElems.length - 1; i >= 0; i--) {
                stackMsg.append('\n').append(stackElems[i].toString());
            }
        }
        return stackMsg.toString();

    }

    @Deprecated(since = "1.0", forRemoval = true)
    @Override
    public Instant getInstant() {
        return super.getInstant();
    }

    @Deprecated(since = "1.0", forRemoval = true)
    @Override
    public String getLoggerName() {
        return super.getLoggerName();
    }

    public String getLoggerShortName() {
        return loggerShortName;
    }

    public String getLogSource() {
        return logSource;
    }

    public List<String> getMessageParams() {
        return messageParams;
    }

    @Deprecated(since = "1.0", forRemoval = true)
    @Override
    public Object[] getParameters() {
        return messageParams.toArray();
    }

    public Event getProtoData() {
        final var builder = EventRecordProto.Event.newBuilder();
        builder.addAllMessageParams(messageParams)
                .setPseudoId(getThreadID()).setTime(getMillis())
                .setSequence(getSequenceNumber()).setMessage(getMessage())
                .setCorrelationId(correlationId)
                .setEventTypeValue(eventType.id)
                .setErrorLevelValue(getLevel().intValue() / 10)
                .setEventSensitivityValue(eventSensitivity.id)
                .setEventSource(logSource);
        final var eTrace = getExceptionTrace();
        if (eTrace != null) {
            builder.setExceptionTrace(eTrace);
        }
        if (resourceReference != null) {
            builder.setResourceRef(resourceReference);
        }
        return builder.build();
    }

    @Deprecated(since = "1.0", forRemoval = true)
    @Override
    public ResourceBundle getResourceBundle() {
        return super.getResourceBundle();
    }

    @Deprecated(since = "1.0", forRemoval = true)
    @Override
    public String getResourceBundleName() {
        return resourceReference;
    }

    public String getResourceReference() {
        return resourceReference;
    }

    public EventLevel getSeverityLevel() {
        return eventLevel;
    }

    @Deprecated(since = "1.0", forRemoval = true)
    @Override
    public String getSourceClassName() {
        return super.getSourceClassName();
    }

    @Deprecated(since = "1.0", forRemoval = true)
    @Override
    public String getSourceMethodName() {
        return super.getSourceMethodName();
    }

    @Deprecated(since = "1.0", forRemoval = true)
    @Override
    public Throwable getThrown() {
        return thrown;
    }

    @Deprecated(since = "1.0", forRemoval = true)
    @Override
    public void setInstant(
            final Instant pInstant) {
        super.setInstant(pInstant);
    }

    @Deprecated(since = "1.0", forRemoval = true)
    @Override
    public void setLevel(
            final Level pLevel) {
        throw new IncompatibleClassChangeError(
                "You Cannot modify value one set");
    }

    @Deprecated(since = "1.0", forRemoval = true)
    @Override
    public void setLoggerName(
            final String pName) {
        super.setLoggerName(pName);
    }

    @Deprecated(since = "1.0", forRemoval = true)
    @Override
    public void setMessage(
            final String pMessage) {
        super.setMessage(pMessage);
    }

    @Deprecated(since = "1.0", forRemoval = true)
    @Override
    public void setParameters(
            final Object[] pParameters) {
        messageParams.clear();
        if (pParameters != null) {
            for (final Object object : pParameters) {
                messageParams.add(object.toString());
            }
        }
        super.setParameters(pParameters);
    }

    @Deprecated(since = "1.0", forRemoval = true)
    @Override
    public void setResourceBundle(
            final ResourceBundle pBundle) {
        super.setResourceBundle(pBundle);
    }

    @Deprecated(since = "1.0", forRemoval = true)
    @Override
    public void setResourceBundleName(
            final String pName) {
        super.setResourceBundleName(pName);
    }

    @Deprecated(since = "1.0", forRemoval = true)
    @Override
    public void setSequenceNumber(
            final long pSeq) {
        super.setSequenceNumber(pSeq);
    }

    @Deprecated(since = "1.0", forRemoval = true)
    @Override
    public void setSourceClassName(
            final String pSourceClassName) {
        super.setSourceClassName(pSourceClassName);
    }

    @Deprecated(since = "1.0", forRemoval = true)
    @Override
    public void setSourceMethodName(
            final String pSourceMethodName) {
        super.setSourceMethodName(pSourceMethodName);
    }

    @Deprecated(since = "1.0", forRemoval = true)
    @Override
    public void setThreadID(
            final int pThreadID) {
        super.setThreadID(pThreadID);
    }

    @Deprecated(since = "1.0", forRemoval = true)
    @Override
    public void setThrown(
            final Throwable pThrown) {
        thrown = pThrown;
        super.setThrown(pThrown);
    }

}
