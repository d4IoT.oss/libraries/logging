// Copyright 2021 Lloyd Fernandes <lloyd-oss@astute.biz>
// SPDX-License-Identifier: GPL-3.0-or-later WITH Classpath-exception-2.0
// SPDX-License-Identifier: LicenseRef-d4iot-oss-derived-license
package com.d4iot.oss.corelib.logging.logger;

import java.lang.management.ManagementFactory;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import com.d4iot.oss.corelib.logging.logger.impl.EventLoggerImpl;

/**
 *
 */
public class EventManager extends LogManager {

    private static String Id =
            ManagementFactory.getRuntimeMXBean().getName() + "^"
                    + System.getProperty("EventManager.Id", "UNKNOWN")
                    + "^";

    private String serviceName =
            System.getProperty("EventManager.Id", "UNKNOWN");

    public EventLogger addLogger(
            final EventLogger pLogger) {
        EventLoggerImpl result = null;

        while (result == null) {
            if (super.addLogger(pLogger.getImplDelegate())) {
                return pLogger;
            }
            result = (EventLoggerImpl) getLogger(pLogger.getImplDelegate()
                    .getName());
        }
        return result.getEventLogger();
    }

    @Override
    public boolean addLogger(
            final Logger pLogger) {
        super.addLogger(new EventLogger(pLogger).getImplDelegate());
        return false;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(
            final String pServiceName) {
        serviceName = pServiceName;
        Id = ManagementFactory.getRuntimeMXBean().getName() + "^"
                + serviceName + "^";

    }

    public static EventManager getEventManager() {
        return (EventManager) LogManager.getLogManager();
    }

    public static String getId() {
        return Id;
    }
}
