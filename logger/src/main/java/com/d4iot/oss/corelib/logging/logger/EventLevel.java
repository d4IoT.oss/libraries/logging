// Copyright 2021 Lloyd Fernandes <lloyd-oss@astute.biz>
// SPDX-License-Identifier: GPL-3.0-or-later WITH Classpath-exception-2.0
// SPDX-License-Identifier: LicenseRef-d4iot-oss-derived-license
package com.d4iot.oss.corelib.logging.logger;

import java.util.logging.Level;

import com.d4iot.oss.corelib.logging.logger.EventRecord.EventLevelClass;

public enum EventLevel {
    UNKNOWN_LEVEL(0, "X"), TRACE(40, "T"), DEBUG(50, "T"), CONFIG(70, "C"),
    INFO(80, "I"), WARNING(90, "W"), ERROR(100, "E"), PANIC(120, "P"),
    METRIC(200, "M");

    public final int id;
    public final Level level;
    public final String shortName;

    EventLevel(final int pId, final String pShortName) {
        id = pId;
        level = new EventLevelClass(name(), id * 10);
        shortName = pShortName;

    }

    public static EventLevel getEventLevel(
            final Level pLevel) {
        final var lvlVal = pLevel.intValue();

        if (lvlVal >= 1200) {
            return PANIC;
        }
        if (lvlVal >= 1000) {
            return ERROR;
        }
        if (lvlVal >= 900) {
            return WARNING;
        }
        if (lvlVal >= 800) {
            return INFO;
        }
        if (lvlVal >= 700) {
            return CONFIG;
        }
        if (lvlVal >= 500) {
            return DEBUG;
        }
        if (lvlVal >= 100) {
            return TRACE;
        }
        return UNKNOWN_LEVEL;
    }
}
